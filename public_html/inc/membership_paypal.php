<?php

/*----------------------------------------------------------
	PROCESS PAYPAL
-----------------------------------------------------------*/

//	ANNUAL MEMBERSHIP SUBSCRIPTION

		if( $form_renewal_option == "Annual" && $form_payment_method == "Subscription" ) {

			if( $form_member_option == "Individual" ) {
				$mform = "ma_25";
				$pp_btn_id = "AZNRG9ENL346G";
			}
			if( $form_member_option == "Family" ) {
				$mform = "ma_40";
				$pp_btn_id = "EDA8PH4TYNTFL";
			}
		}

//	ANNUAL MEMBERSHIP

		if( $form_renewal_option == "Annual" && $form_payment_method == "PayPal" ) {

			if( $form_member_option == "Student" ) {
				$mform = "ma1_5";
				$pp_btn_id = "C6C4R6MPSW6JE";
			}
			if( $form_member_option == "Individual" ) {
				$mform = "ma1_25";
				$pp_btn_id = "HUYW2H57XZLNQ";
			}
			if( $form_member_option == "Family" ) {
				$mform = "ma1_40";
				$pp_btn_id = "FZLVSG3RPMMAJ";
			}
		}

//	LIFETIME MEMBERSHIP

		if( $form_renewal_option == "Lifetime" ) {

			if( $form_member_option == "Individual" ) {
				$mform = "ml_225";
				$pp_btn_id = "D66S5UTXC3M2U";
			}
			if( $form_member_option == "Family" ) {
				$mform = "ml_400";
				$pp_btn_id = "J7QZZE2QWYDLC";
			}
		}

//	SUBMIT PAYPAL FORM

?>

<form name="<?php echo $mform; ?>" id="<?php echo $mform; ?>" action="https://www.paypal.com/cgi-bin/webscr" method="post">
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="<?php echo $pp_btn_id; ?>">
<img alt="" border="0" src="https://www.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
</form>

<script language="javascript">document.getElementById('<?php echo $mform; ?>').submit()</script>
