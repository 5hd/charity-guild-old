<?php

/*=============================================================
	DEFAULTS
==============================================================*/

$errors_exist = 0;

$error = array();

$error_msg   = "";
$success_msg = "";
$warning_msg = "";
$eresult_msg = "";

/*=============================================================
	PROCESS THE EMAIL FORM
==============================================================*/

if( isset( $_POST['esubmit'] ) ) {

	$first_name    = trim( $_POST['first_name'] );
	$last_name     = trim( $_POST['last_name'] );
	$email_address = trim( $_POST['email_address'] );
	$subject       = trim( stripslashes( $_POST['subject'] ) );
	$message       = trim( strip_tags( stripslashes( html_entity_decode( $_POST['message'], ENT_XHTML, 'UTF-8' ) ) ) );

	if( empty( $first_name ) ) {
		$errors_exist++;
		$error['first_name'] = true;
	}
	if( empty( $last_name ) ) {
		$errors_exist++;
		$error['last_name'] = true;
	}
	if( empty( $email_address ) ) {
		$errors_exist++;
		$error['email_address'] = true;
	}
	if( empty( $subject ) ) {
		$errors_exist++;
		$error['subject'] = true;
	}
	if( empty( $message ) ) {
		$errors_exist++;
		$error['message'] = true;
	}

	if( $_POST["g-recaptcha-response"] ) {
		$response = $reCaptcha->verifyResponse( $_SERVER["REMOTE_ADDR"], $_POST["g-recaptcha-response"] );
	}

	if( !$response->success ) {
		$errors_exist++;
		$error['e-recaptcha'] = true;
	}

	if( $errors_exist ) {
		$error_msg .= "<span>Please correct the errors marked in red.</span><br />\n";
	}
}

/*=============================================================
	PREPARE AND SEND EMAILS
==============================================================*/

if( isset( $_POST['esubmit'] ) && !$errors_exist ) {

//	TEXT MESSAGE

	$text_sender_info = "\n\n" . $first_name . "\n" . $last_name . "\n" . $email_addr;
	$text_server_msg  = "\n\n\n\n__________\n\nThis email was submitted from thecharityguid.org. The sender's server address is " . gethostbyaddr( $_SERVER['REMOTE_ADDR'] ) . ".\n\n";

	$text_message = ""
		. date('l, F j, Y') . "\n\n"
		. $message
		. $text_sender_info
		. $text_server_msg;

//	HTML MESSAGE

	$html_sender_info = "<p>" . $first_name . " " . $last_name . "<br />" . $email_address . "</p>\n";
	$html_server_msg  = "<p><br /></p>\n<p>__________<br /><br />This email was submitted from <a href='http://thecharityguild.org'>thecharityguid.org</a>. The sender's server address is " . gethostbyaddr( $_SERVER['REMOTE_ADDR'] ) . ".</p>\n";

	$html_message = ""
		. "<p>" . nl2br( $message ) . "</p>\n\n"
		. "<br />"
		. $html_sender_info
		. "<div style=\"font-family: arial, sans-serif; font-size: small; line-height: 100%;\">\n"
		. $html_server_msg . ""
		. "</div>\n\n";

//	SEND EMAILS

	$user_msg = 1;

	$user_text_message    = $text_message;
	$user_html_message    = $html_message;
	$user_subject         = $subject;
	$user_to_name         = stripslashes( $first_name ) . " " . stripslashes( $last_name );
	$user_email_address   = $email_address;
	$user_error_message   = "An error occurred and a CC to " . $email_address . " failed.";
	$user_success_message = "Your email was sent to The Charity Guild with a CC to " . $email_address . ".";

	$cg_text_message  = $text_message;
	$cg_html_message  = $html_message;
	$cg_subject       = $subject;
	$cg_to_name       = array( "The Charity Guild" );
	$cg_to_email      = array( "info@thecharityguild.org" );
	$cg_reply_name    = stripslashes( $first_name ) . " " . stripslashes( $last_name );
	$cg_reply_address = $email_address;
	$cg_error_message = "An error occurred and The Charity Guild did not receive your email.";

include( 'do_email.php' );

	if( !$errors_exist ) {
		$first_name    = "";
		$last_name     = "";
		$email_address = "";
		$subject       = "";
		$message       = "";
	}
}

/*-------------------------------------------------------------
	FORMAT RESULT MESSAGES, IF ANY
--------------------------------------------------------------*/

$eresult_msg .= $error_msg   > "" ? "<p class='error'>"   . $error_msg   . "</p>\n" : "";
$eresult_msg .= $warning_msg > "" ? "<p class='warning'>" . $warning_msg . "</p>\n" : "";
$eresult_msg .= $success_msg > "" ? "<p class='success'>" . $success_msg . "</p>\n" : "";

echo "\n";

?>
