<?php
/*---------------------------------------------------------------------
 		REQUIRED VARIABLES:
-----------------------------------------------------------------------

		$home_url = for images

		-----------
		USER EMAIL:
		-----------

		$user_msg = 0 or 1
		$user_text_message    =
		$user_html_message    =
		$user_subject         =
		$user_to_name         =
		$user_email_address   =
		$user_error_message   = error message on fail / 0 for no message
		$user_success_message = message on success / 0 for no message

		---------
		CG EMAIL:
		---------

		$cg_text_message   =
		$cg_html_message   =
		$cg_subject        =
		$cg_reply_name     =
		$cg_reply_address  =
		$cg_to_name        = array
		$cg_to_email       = array
		$cg_error_message  = error message on fail / 0 for no message

----------------------------------------------------------------------*/

//	EMAIL ADDRESSES

		$cg_email_address = "info@thecharityguild.org";
		$webmaster_email_address = "sonya@briteventures.com";

//	TEXT MESSAGES

		$cg_text_msg = $cg_text_message;

		if( $user_msg ) {
			$user_text_msg = $user_text_message;
		}

//	HTML EMAIL HEADER

		$cg_email_width  = "<div style=\"margin: 0 auto; padding: 0; width: 100%;  max-width: 600px;\">\n\n";
		$cg_email_square = "<div style=\"margin: 0 auto; width: 300px; background-color: #358f5b; border-radius: 15px; \"><p style=\"margin: 0 auto; padding: 15px; font-family: arial; font-weight: bold; color: white; text-align: center;\"><span style=\"font-size: large; font-variant: small-caps;\">The Charity Guild, Inc.</span><br />Food Pantry and Thrift Shop</p></div>";
		$cg_email_line   = "<p style=\"width: 100%; height: 10px; background-color: #358f5b;\">&nbsp;</p>\n";

		$cg_greenhd_style = "margin: 25px auto; padding: 0; width: 100%; line-height: 125%; color: #358f5b; font-size: x-large; font-weight: bold; font-family: times, serif; text-align: center; font-variant: small-caps;";
		$cg_email_greenhd = "<p style=\"" . $cg_greenhd_style . "\">" . $cg_subject . "</p>\n";
		$user_email_greenhd = "<p style=\"" . $cg_greenhd_style . "\">" . $user_subject . "</p>\n";

		$cg_email_header  = $cg_email_width;
		$cg_email_header .= $cg_email_square;
		$cg_email_header .= $cg_email_greenhd;
		$cg_email_header .= $cg_email_line;

		$user_email_header  = $cg_email_width;
		$user_email_header .= $cg_email_square;
		$user_email_header .= $user_email_greenhd;
		$user_email_header .= $cg_email_line;

//	HTML EMAIL FOOTER

		$cg_fb_btn = "<a title=\"Facebook\" href=\"https://www.facebook.com/thecharityguild/\" target=\"_blank\"><img style=\"width: 35px; height: 35px;\" src=\"" . $home_url . "/img/facebook-icon.png\" /></a>";
		$cg_instagram_btn = "<a title=\"Instagram\" href=\"https://instagram.com/thecharityguild\" target=\"_blank\"><img style=\"width: 35px; height: 35px;\" src=\"" . $home_url . "/img/instagram-icon.png\" /></a>";
		$cg_ebay_btn = "<a title=\"Ebay\" href=\"https://www.charity.ebay.com/charity/The-Charity-Guild/193554\" target=\"_blank\"><img style=\"width: 35px; height: 35px;\" src=\"" . $home_url . "/img/ebay-icon.png\" /></a>";
		$cg_flickr_btn = "<a title=\"Flickr\" href=\"https://www.flickr.com/photos/thecharityguild/sets/\" target=\"_blank\"><img style=\"width: 35px; height: 35px;\" src=\"" . $home_url . "/img/flickr-icon.png\" /></a>";

		$cg_email_footer = ""
			. $cg_email_line
			. "<br />\n"
			. "<div style=\"margin: 0 auto; width: 100%; text-align: center;\">\n"
			. "<table style=\"margin: 0 auto; \"><tr>\n"
			. "<td style=\"width: 50px; text-align: center;\">" . $cg_fb_btn . "</td>\n"
			. "<td style=\"width: 50px; text-align: center;\">" . $cg_instagram_btn . "</td>\n"
			. "<td style=\"width: 50px; text-align: center;\">" . $cg_ebay_btn . "</td>\n"
			. "<td style=\"width: 50px; text-align: center;\">" . $cg_flickr_btn . "</td>\n"
			. "</tr></table>\n\n"
			. "<p style=\"color: #358f5b; font-weight: bold;\"><span style=\"color: #358f5b; font-family: times, serif; font-size: large; font-weight: bold; font-variant: small-caps;\">The Charity Guild, Inc.</span><br />501 Main Street<br />P.O. Box 4856<br />Brockton, MA 02303</p>\n"
			. "<p style=\"color: #358f5b; font-weight: bold;\">(508) 583-5280<br />\n" . "<a href=\"https://thecharityguild.org\">thecharityguild.org</a></p>\n"
			. "</div>\n\n";

//	HTML EMAIL MESSAGES

		$cg_html_msg = ""
			. $cg_email_header
			. "<br />\n"
			. "<div style=\"font-family: arial, sans-serif; font-size: medium; line-height: 150%;\">\n"
			. "<p>" . date('l, F j, Y') . "<br /><br /></p>\n\n"
			. $cg_html_message . "\n"
			. "<br />\n"
			. "</div>\n\n"
			. $cg_email_footer;

		$user_html_msg = ""
			. $user_email_header
			. "<br />\n"
			. "<div style=\"font-family: arial, sans-serif; font-size: medium; line-height: 150%;\">\n"
			. "<p>" . date('l, F j, Y') . "<br /><br /></p>\n\n"
			. $user_html_message . "\n"
			. "<br />\n"
			. "</div>\n\n"
			. $cg_email_footer;

//	SEND USER EMAIL

		if( $user_msg ) {

			$from_name  = "The Charity Guild";
			$from_email = $cg_email_address;

			$reply_name  = "The Charity Guild";
			$reply_email = $cg_email_address;

			$to_name    = $user_to_name;
			$to_email   = $user_email_address;

			$bccName  = "Brite Ventures";
			$bccEmail = "sonya@briteventures.com";

			$mail = new PHPMailer;

			$mail->From = $from_email;
			$mail->FromName = $from_name;
			$mail->addAddress( $to_email, $to_name );
			$mail->addReplyTo( $reply_email, $reply_name );

			$mail->isHTML(true);
			$mail->Subject = $user_subject;
			$mail->Body    = $user_html_msg;
			$mail->AltBody = $user_text_msg;

			if( $mail->send() ) {

				if( $user_success_message ) {
					$success_msg .= "<span>" . $user_success_message . "</span><br />\n";
				}

			} else {

				if( $user_error_message ) {
					$errors_exist++;
					$error_msg .= "<span>" . $user_error_message . "</span><br />\n";
				}
			}
		}

//	SEND CG EMAIL(S)

		$from_name  = "The Charity Guild Website";
		$from_email = $cg_email_address;

		$reply_name  = $cg_reply_name;
		$reply_email = $cg_reply_address;

		$mail = new PHPMailer;

		$mail->From = $from_email;
		$mail->FromName = $from_name;

		for( $i = 0; $i < count( $cg_to_name ); $i++ ) {
			$mail->addAddress( $cg_to_email[$i], $cg_to_name[$i] );
		}

		$mail->addReplyTo( $reply_email, $reply_name );
		$mail->isHTML(true);
		$mail->Subject = $cg_subject;
		$mail->Body    = $cg_html_msg;
		$mail->AltBody = $cg_text_msg;

		if( !$mail->send() ) {

			if( $cg_error_message ) {
				$errors_exist++;
				$error_msg .= "<span>" . $cg_error_message . "</span><br />\n";
			}
		}

?>
