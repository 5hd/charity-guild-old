<?php

/*=============================================================
	DEFAULTS
==============================================================*/

$cg_to_name = array(
	"The Charity Guild",
	"Barbara Janelli",
);

$cg_to_email = array(
	"info@thecharityguild.org",
	"bjanelli1@yahoo.com",
);

$errors_exist = 0;

$merror = array();

$form_renewal_option = "Annual";
$form_annual_option  = "Individual";

$annual_membership_display_class   = "";
$lifetime_membership_display_class = " invisible";

$form_member_bday_mm = "00";
$form_member_bday_dd = "00";

$spouse_name_display_class   = ' class="invisible"';
$business_name_display_class = ' class="invisible"';

$subscription_text_display_class = "";
$paypal_text_display_class       = ' class="invisible"';
$pledge_text_display_class       = ' class="invisible"';

$error_msg   = "";
$warning_msg = "";
$success_msg = "";
$result_msg  = "";

$membership_fee = array(
	"annual student"      => "$5",
	"annual individual"   => "$20",
	"annual family"       => "$35",
	"lifetime individual" => "$225",
	"lifetime family"     => "$400",
);

$bday_month = array(
	1  => "January ",
	2  => "February ",
	3  => "March ",
	4  => "April ",
	5  => "May ",
	6  => "June ",
	7  => "July ",
	8  => "August ",
	9  => "September ",
	10 => "October ",
	11 => "November ",
	12 => "December ",
);

/*=============================================================
	RETURN FROM PLEDGE RECEIPT
==============================================================*/

if( isset( $_GET['mpledge'] ) ) {
	$success_msg .= "<span>Thank you for your Membership Pledge. Check your email for a confirmation from The Charity Guild.</span><br />";
}

/*=============================================================
	RETURN FROM PAYPAL
==============================================================*/

if( isset( $_GET['mp'] ) ) {

	$pp_stat = $_GET['mp'];

	if( $pp_stat == "c" ) {

		$error_msg .= "<span>Your Membership PayPal Payment was canceled.</span><br />";
		$error_msg .= "<span>Please try again or select Pledge if you would prefer to pay by check.</span><br />";
		$error_msg .= "<span>Thank you.</span><br />";

		$cg_to_name       = "Membership at The Charity Guild";
		$cg_reply_name    = "No Reply";
		$cg_reply_address = "noreply@thecharityguild.org";
		$cg_subject       = "Canceled Membership PayPal Payment";
		$cg_error_message = 0;

		$cg_text_message = ""
			. date('l, F j, Y') . "\n\n"
			. "This is to notify you that the most recent Membership submission did not complete their PayPal transaction." . "\n\n"
			. "Regarding the previous email with the subject NEW CHARITY GUILD MEMBERSHIP: Keep your eyes peeled for completion of this membership by PayPal or check." . "\n\n";

		$cg_html_message = ""
			. "<p>This is to notify you that the most recent Membership submission did not complete their PayPal transaction.</p>" . "\n\n"
			. "<p>Regarding the previous email with the subject NEW CHARITY GUILD MEMBERSHIP: Keep your eyes peeled for completion of this membership by PayPal or check.</p>" . "\n\n";

		$user_msg = 0;

include('do_email.php');

	}

	if ($pp_stat == 's') {
		$success_msg .= "<span>Thank you for your Membership submission!<br />Check your email for confirmations from The Charity Guild and PayPal.</span><br />";
	}
}

/*=============================================================
	PROCESS THE MEMBERSHIP FORM
==============================================================*/

if( isset( $_POST['msubmit'] ) ) {

	$form_new_renew_option = $_POST['form_new_renew_option'];
	$form_renewal_option   = $_POST['form_renewal_option'];
	$form_annual_option    = $_POST['form_annual_option'];
	$form_lifetime_option  = $_POST['form_lifetime_option'];
	$form_member_title     = trim( $_POST['form_member_title'] );
	$form_member_firstname = trim( $_POST['form_member_firstname'] );
	$form_member_lastname  = trim( $_POST['form_member_lastname'] );
	$form_member_bday_mm   = intval( $_POST['form_member_bday_mm'] );
	$form_member_bday_dd   = intval( $_POST['form_member_bday_dd'] );
	$form_spouse_title     = trim( $_POST['form_spouse_title'] );
	$form_spouse_firstname = trim( $_POST['form_spouse_firstname'] );
	$form_spouse_lastname  = trim( $_POST['form_spouse_lastname'] );
	$form_business_name    = trim( $_POST['form_business_name'] );
	$form_member_address1  = trim( $_POST['form_member_address1'] );
	$form_member_address2  = trim( $_POST['form_member_address2'] );
	$form_member_city      = trim( $_POST['form_member_city'] );
	$form_member_state     = $_POST['form_member_state'];
	$form_member_zip       = trim( $_POST['form_member_zip'] );
	$form_member_phone     = trim( $_POST['form_member_phone'] );
	$form_member_alt_phone = trim( $_POST['form_member_alt_phone'] );
	$form_member_emailaddr = trim( $_POST['form_member_emailaddr'] );
	$form_payment_method   = $_POST['form_payment_method'];

	if( empty( $form_member_firstname ) ) {
		$errors_exist++;
		$merror['form_member_firstname'] = true;
	}
	if( empty( $form_member_lastname ) ) {
		$errors_exist++;
		$merror['form_member_lastname'] = true;
	}
	if( !empty( $form_member_firstname ) && !empty( $form_member_lastname ) ) {

		if( empty( $form_member_title ) ) {
			$member_name = stripslashes( $form_member_firstname ) . " " . stripslashes( $form_member_lastname );
		} else {
			$member_name = $form_member_title . " " . stripslashes( $form_member_firstname ) . " " . stripslashes( $form_member_lastname );
		}
	}
	if( $form_member_bday_mm > 0 || $form_member_bday_dd > 0 ) {

		if( $form_member_bday_mm == 0 || $form_member_bday_dd == 0 ) {
			$errors_exist++;
			$merror['form_member_birthday'] = true;

		} else {

			switch( $form_member_bday_mm ) {
				case 2 :
					if( $form_member_bday_dd > 29 ) {
						$errors_exist++;
						$merror['form_member_birthday'] = true;
					}
					break;
				case 4 :
					if( $form_member_bday_dd > 30 ) {
						$errors_exist++;
						$merror['form_member_birthday'] = true;
					}
					break;
				case 6 :
					if( $form_member_bday_dd > 30 ) {
						$errors_exist++;
						$merror['form_member_birthday'] = true;
					}
					break;
				case 9 :
					if( $form_member_bday_dd > 30 ) {
						$errors_exist++;
						$merror['form_member_birthday'] = true;
					}
					break;
				case 11 :
					if( $form_member_bday_dd > 30 ) {
						$errors_exist++;
						$merror['form_member_birthday'] = true;
					}
					break;
			}
		}
	}
	if( $form_renewal_option == "Annual" ) {
		$annual_membership_display_class   = "";
		$lifetime_membership_display_class = " invisible";
		$form_member_option = $form_annual_option;
	}
	if( $form_renewal_option == "Lifetime" ) {
		$annual_membership_display_class   = " invisible";
		$lifetime_membership_display_class = "";
		$form_member_option = $form_lifetime_option;
	}
	if( $form_member_option == "Family" ) {

		if( empty( $form_spouse_firstname ) ) {
			$errors_exist++;
			$merror['form_spouse_firstname'] = true;
		}
		if( empty( $form_spouse_lastname ) ) {
			$errors_exist++;
			$merror['form_spouse_lastname'] = true;
		}
		if( !empty( $form_spouse_firstname ) && !empty( $form_spouse_lastname ) ) {

			if( empty( $form_spouse_title ) ) {
				$spouse_name = $form_spouse_firstname . " " . $form_spouse_lastname;
			} else {
				$spouse_name = $form_spouse_title . " " . $form_spouse_firstname . " " . $form_spouse_lastname;
			}
		}
	}

	if( empty( $form_member_address1 ) ) {
		$errors_exist++;
		$merror['form_member_address1'] = true;
	}
	if( empty( $form_member_city ) ) {
		$errors_exist++;
		$merror['form_member_city'] = true;
	}
	if( empty( $form_member_zip ) ) {
		$errors_exist++;
		$merror['form_member_zip'] = true;
	} else {

		if( !is_numeric( $form_member_zip ) ) {
			$errors_exist++;
			$merror['form_member_zip'] = true;
		}
	}

	if( empty( $form_member_phone ) ) {
		$errors_exist++;
		$merror['form_member_phone'] = true;
	}
	if( empty( $form_member_emailaddr ) ) {
		$errors_exist++;
		$merror['form_member_emailaddr'] = true;
	} else {

		if( FALSE == filter_var( $form_member_emailaddr, FILTER_VALIDATE_EMAIL ) ) {
			$errors_exist++;
			$merror['form_member_emailaddr'] = true;
		}
	}

	switch( $form_payment_method ) {
		case "Subscription" :
			$subscription_text_display_class = "";
			$paypal_text_display_class       = ' class="invisible"';
			$pledge_text_display_class       = ' class="invisible"';
			break;
		case "PayPal" :
			$subscription_text_display_class = ' class="invisible"';
			$paypal_text_display_class       = "";
			$pledge_text_display_class       = ' class="invisible"';
			break;
		case "Pledge" :
			$subscription_text_display_class = ' class="invisible"';
			$paypal_text_display_class       = ' class="invisible"';
			$pledge_text_display_class       = "";
			break;
		default :
			$subscription_text_display_class = "";
			$paypal_text_display_class       = ' class="invisible"';
			$pledge_text_display_class       = ' class="invisible"';
			break;
	}

	if( $_POST["g-recaptcha-response"] ) {
		$response = $reCaptcha->verifyResponse( $_SERVER["REMOTE_ADDR"], $_POST["g-recaptcha-response"] );
	}

	if( !$response->success ) {
		$errors_exist++;
		$error['m-recaptcha'] = true;
	}

	if( $errors_exist ) {
		$receipt = 0;
		$error_msg .= "Please correct the errors marked in red.";
	} else {

/*=============================================================
	PREPARE AND SEND EMAILS
==============================================================*/

		if( $form_new_renew_option == "New Membership" ) {
			$member_subject = $form_payment_method == "Pledge" ? "Become a Member of The Charity Guild!" : "Welcome to The Charity Guild!";
			$appnoun = "Application";
		} else {
			$member_subject = "Your Charity Guild Membership Renewal";
			$appnoun = "Renewal";
		}

		$cg_subject = "Membership Form Submitted";

		$fee_key = strtolower( $form_renewal_option ) . " " . strtolower( $form_member_option );

		if( $form_member_bday_mm > 0 && $form_member_bday_dd > 0 ) {
			$the_bday = $bday_month[ $form_member_bday_mm ] . $form_member_bday_dd;
		} else {
			$the_bday = false;
		}

/*----------------------------------------------------------
 	PREPARE TEXT EMAILS
-----------------------------------------------------------*/

//	TEXT MEMBER INFO

		$text_member_info = ""
			. "MEMBERSHIP OPTIONS:\n\n"
			. "Submission Type: " . $form_new_renew_option . "\n\n"
			. "Renewal Option: " . $form_renewal_option . "\n"
			. "Member Option: " . $form_member_option . "\n\n"
			. $form_renewal_option . " " . $form_member_option . " Membership Fee: " . $membership_fee[$fee_key] . "\n\n\n";

		$text_member_info .= "NAME AND ADDRESS:\n\n";

		$text_member_info = stripslashes( $member_name ) . "\n";

		if( $form_member_option == "Family" ) {
			$text_member_info .= $spouse_name . "\n";
		}

		$text_member_info .= stripslashes( $form_member_address1 ) . "\n";

		if( $form_member_address2 > "" ) {
			$text_member_info .= stripslashes( $form_member_address2 ) . "\n";
		}

		$text_member_info .= stripslashes( $form_member_city ) . ", " . $form_member_state . " " . $form_member_zip . "\n\n\n";

		if( $the_bday ) {

			$text_member_info .= "BIRTHDAY:\n\n";

			if( $form_member_option == "Family" ) {
				$text_member_info .= stripslashes( $member_name ) . "'s birthday is " . $the_bday . "\n\n";
			} else {
				$text_member_info .= $the_bday . "\n\n";
			}
		}

		$text_member_info .= ""
			. "PHONE AND EMAIL:\n\n"
			. $form_member_phone . "\n"
			. $form_member_alt_phone > "" ? $form_member_alt_phone . "\n" : ""
			. $form_member_emailaddr . "\n\n\n";

		$text_member_info .= "PAYMENT METHOD:\n\n";

		switch( $form_payment_method ) {

			case "Subscription" :
				$text_member_info .= "PayPal Automatic Annual Renewal" . "\n\n\n";
				break;
			case "PayPal" :
				$text_member_info .= "PayPal" . "\n\n\n";
				break;
			case "Pledge" :
				$text_member_info .= "Pledge - Print the Membership " . $appnoun . " (or this email) and mail in a check." . "\n\n\n";
				break;
		}

//	TEXT MEMBER BENEFITS

		$text_member_benefits = ""
			. "Being a Member of The Charity Guild makes you an important part of the work we do in the community." . "\n\n"
			. "As a Member of The Charity Guild, you will be kept up-to-date about our events and projects. You will receive our seasonal newsletter and invitations to special events such as the annual Harvest Gala." . "\n\n";

//	TEXT CG MESSAGE

		$text_cg_msg = ""
			. date('l, F j, Y') . "\n\n"
			. "This is to notify you that " . stripslashes( $member_name ) . " submitted the following Membership " . $appnoun . " from The Charity Guild`s website." . "\n\n"
			. $text_member_info;

//	TEXT MEMBER MESSAGE

		$text_member_msg = ""
			. "Dear " . stripslashes( $form_member_firstname ) . "," . "\n\n";

		if( $form_payment_method == "Pledge" ) {

			$text_member_msg .= ""
				. "We look forward to receiving your Membership info in the mail." . "\n\n"
				. $text_member_benefits
				. "If you did not print your Membership " . $appnoun . " at our website, please use a printout of this email instead.\n\n"
				. "Send your Membership " . $appnoun . " and Fee to:\n\n"
				. "\t\t" . "The Charity Guild, Inc." . "\n"
				. "\t\t" . "P.O. Box 4856" . "\n"
				. "\t\t" . "Brockton, MA 02303-4856" . "\n\n"
				. "Please feel free to contact us if you have any questions." . "\n\n";

		} else {

			$text_member_msg .= ""
				. $form_new_renew_option == "New Membership" ? "Thank you for becoming a Member of The Charity Guild!" . "\n\n" : "Thank you for renewing your Charity Guild Membership!" . "\n\n"
				. $text_member_benefits
				. "Please feel free to contact us if you have any questions." . "\n\n"
				. $form_new_renew_option == "New Membership" ? "Welcome to The Charity Guild!" . "\n\n" : "Welcome back " . ucwords($form_member_firstname) . "!" . "\n\n";
		}

		$text_member_msg .= ""
			. "Sincerely,\n\n"
			. "The Charity Guild, Inc." . "\n" . "(508) 583-5280" . "\n" . "http://thecharityguild.org" . "\n\n\n"
			. "This confirms the information you entered at our website:" . "\n\n"
			. $text_member_info;

/*----------------------------------------------------------
 	PREPARE HTML EMAILS
-----------------------------------------------------------*/

//	HTML EMAIL HEADERS

		if( $form_new_renew_option == "New Membership" ) {

			$appnoun = "Application";

			if( $form_payment_method == "Pledge" ) {
				$member_subject = "BECOME A MEMBER OF THE CHARITY GUILD!";
			} else {
				$member_subject = "WELCOME TO THE CHARITY GUILD!";
			}

		} else {
			$appnoun = "Renewal";
			$member_subject = "WELCOME BACK TO THE CHARITY GUILD!";
		}

		$cg_subject = $form_new_renew_option == "New Membership" ? "NEW MEMBERSHIP" : "MEMBERSHIP RENEWAL";

//	HTML MEMBER INFO

		$html_member_info = "<br />\n"
			. "<p style=\"color: #358f5b; font-family: arial, sans-serif; font-size: large; font-weight: bold;\">Membership Options:</p>\n"
			. "<p>Submission Type: " . $form_new_renew_option . "<br /><br />\n"
			. "Renewal Option: " . $form_renewal_option . "<br />\n"
			. "Member Option: " . $form_member_option . "<br /><br />\n"
			. $form_renewal_option . " " . $form_member_option . " Membership Fee: " . $membership_fee[$fee_key] . "</p>\n";

		$html_member_info .= "<br />\n<p style=\"color: #358f5b; font-family: arial, sans-serif; font-size: large; font-weight: bold;\">Name and Address:</p>\n";

		$html_member_info .= "<p>" . stripslashes( $member_name ) . "<br />\n";

		if( $form_member_option == "Family" ) {
			$html_member_info .= $spouse_name . "<br />\n";
		}

		$html_member_info .= stripslashes( $form_member_address1 ) . "<br />\n";

		if( $form_member_address2 > "" ) {
			$html_member_info .= stripslashes( $form_member_address2 ) . "<br />\n";
		}

		$html_member_info .= stripslashes( $form_member_city ) . ", " . $form_member_state . " " . $form_member_zip . "</p>\n\n";

		if( $the_bday ) {

			$html_member_info .= "<br />\n<p style=\"color: #358f5b; font-family: arial, sans-serif; font-size: large; font-weight: bold;\">Birthday:</p>\n";

			if( $form_member_option == "Family" ) {
				$html_member_info .= "<p>" . stripslashes( $member_name ) . "'s birthday is " . $the_bday . ".</p>\n\n";
			} else {
				$html_member_info .= "<p>" . $the_bday . "</p>\n\n";
			}
		}

		$html_member_info .= "<br />\n"
			. "<p style=\"color: #358f5b; font-family: arial, sans-serif; font-size: large; font-weight: bold;\">Phone and Email:</p>\n"
			. "<p>" . $form_member_phone . "<br />\n";

		if( $form_member_alt_phone > "" ) {
			$html_member_info .= $form_member_alt_phone . "<br />\n";
		}

		$html_member_info .= "<a href=\"mailto:" . $form_member_emailaddr . "\">" . $form_member_emailaddr . "</a></p>\n\n";

		$html_member_info .= "<br />\n"
			. "<p style=\"color: #358f5b; font-family: arial, sans-serif; font-size: large; font-weight: bold;\">Payment Method:</p>\n";

		switch( $form_payment_method ) {

			case "Subscription" :
				$html_member_info .= "<p>PayPal Automatic Annual Renewal</p>\n\n";
				break;
			case "PayPal" :
				$html_member_info .= "<p>PayPal</p>\n\n";
				break;
			case "Pledge" :
				$html_member_info .= "<p>Pledge - Print the Membership " . $appnoun . " (or this email) and mail in a check.</p>\n\n";
				break;
		}

//	HTML MEMBER BENEFITS

		$html_member_benefits = ""
			. "<p>Being a Member of The Charity Guild makes you an important part of the work we do in the community.</p>\n"
			. "<p>As a Member of The Charity Guild, you will be kept up-to-date about our events and projects. You will receive our seasonal newsletters and invitations to special events.</p>\n\n";

//	HTML CG MESSAGE

		$html_cg_msg .= ""
			. "<p>This is to notify you that " . stripslashes( $member_name ) . " submitted the following Membership " . $appnoun . " from <a href='https://thecharityguild.org' target='_blank'>thecharityguild.org</a>:</p>\n\n"
			. "<hr />\n"
			. $html_member_info
			. "<p><br /></p>\n";

//	HTML MEMBER MESSAGE

		$html_member_msg .= ""
			. "<p>Dear " . stripslashes( $form_member_firstname ) . "," . "</p>\n\n";

		if( $form_payment_method == "Pledge" ) {

			$html_member_msg .= ""
				. "<p>We look forward to receiving your Membership info in the mail.</p>\n\n"
				. $html_member_benefits
				. "<p>If you did not print your Membership " . $appnoun . " at our website, please use a printout of this email instead.</p>\n\n"
				. "<p>Send your Membership " . $appnoun . " and Fee to:</p>\n\n"
				. '<p style="margin-left: 25px;">The Charity Guild, Inc.<br />P.O. Box 4856<br />Brockton, MA 02303-4856</p>' . "\n\n"
				. "<p>Please feel free to contact us if you have any questions.</p>\n\n";

		} else {

			$html_member_thankyou = $form_new_renew_option == "New Membership" ? "<p>Thank you for becoming a Member of The Charity Guild!</p>\n\n" : "<p>Thank you for renewing your Charity Guid Membership!</p>\n\n";
			$html_member_welcome  = $form_new_renew_option == "New Membership" ? "<p>Welcome to The Charity Guild!</p>\n\n" : "<p>Welcome back " . ucwords($form_member_firstname) . "!</p>\n\n";

			$html_member_msg .= ""
				. $html_member_thankyou
				. $html_member_benefits
				. "<p>Please feel free to contact us if you have any questions.</p>\n\n"
				. $html_member_welcome;
		}

		$html_member_msg .= ""
			. "<p>Sincerely,</p>\n\n"
			. "<p style=\"color: #358f5b;\">The Charity Guild, Inc.</p>\n\n"
			. "<p><br /></p>\n"
			. "<hr />\n"
			. "<p>This confirms the information you entered at <a href=\"http://thecharityguild.org\" target=\"_blank\">thecharityguild.org</a>:</p>\n"
			. $html_member_info
			. "<p><br /></p>\n";

/*----------------------------------------------------------
 	SEND EMAILS
-----------------------------------------------------------*/

//	CG EMAIL

		$cg_text_message   = $text_cg_msg;
		$cg_html_message   = $html_cg_msg;
		$cg_subject        = $cg_subject;
		$cg_reply_name     = stripslashes( $member_name );
		$cg_reply_address  = $form_member_emailaddr;
		$cg_error_message  = "An error occurred and your Membership " . $appnoun . " was not sent to The Charity Guild.";

//	MEMBER EMAIL

		$user_msg = 1;
		$user_text_message = $text_member_msg;
		$user_html_message = $html_member_msg;
		$user_subject = $member_subject;
		$user_to_name = stripslashes( $member_name );
		$user_email_address = $form_member_emailaddr;
		$user_error_message = "An error occurred and your Membership Confirmation Email was not sent.  Please contact The Charity Guild to confirm your membership.";
		$user_success_message = $form_new_renew_option == "New Membership" ? "Thank you for becoming a Member of The Charity Guild!" : "Thank you for renewing your Charity Guild Membership!";

include( 'do_email.php' );

	}

	if( !$errors_exist ) {
		$receipt = 1;
	}
}

/*----------------------------------------------------------
 	FORMAT RESULT MESSAGE, IF ANY
-----------------------------------------------------------*/

$mresult_msg .= $error_msg   > "" ? "<p class='error'>"   . $error_msg   . "</p>\n" : "";
$mresult_msg .= $warning_msg > "" ? "<p class='warning'>" . $warning_msg . "</p>\n" : "";
$mresult_msg .= $success_msg > "" ? "<p class='success'>" . $success_msg . "</p>\n" : "";

echo "\n";

?>
