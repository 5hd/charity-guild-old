( function( $ ) {

	$( function () {

//---------------------------------------------------------
// 	ALWAYS USE HTTPS (SECURE SERVER)
//---------------------------------------------------------

		if( window.location.protocol != "https:" ) {

			var thequery = location.search;

			if( thequery > "" ) {
				location.replace( "https://thecharityguild.org" . thequery );
			} else {
				location.replace( "https://thecharityguild.org" );
			}
		}


/*-------------------------------------------------------------------
	NON-JQUERYUI ACCORDION FUNCTIONS
--------------------------------------------------------------------*/

		$( "#accordion h2" ).click( function() {

			if( $( this ).next( ".content" ).is( ":visible" ) ){
				$( this ).children( "span" ).html( "+" );
			} else {
				$( this ).children( "span" ).html( "-" );
			}

			$( this ).next( ".content" ).toggle( "slow" );
		});

		$( "#membership-accordion h3" ).click( function() {

			if( $( this ).next( ".content" ).is( ":visible" ) ){
				$( this ).children( "span" ).html( "+" );
			} else {
				$( this ).children( "span" ).html( "-" );
			}

			$( this ).next( ".content" ).toggle( "slow" );
		});

		$( "a.close" ).click( function( e ) {
			$( this ).closest( "li" ).children( ".content" ).toggle( "slow" );
			e.preventDefault();
		});

		$( "#accordion a.close" ).click( function( e ) {
			$( this ).closest( "li" ).children( "H2" ).children( "span" ).html( "+" );
		});
		$( "#membership-accordion a.close" ).click( function( e ) {
			$( this ).closest( "li" ).children( "H3" ).children( "span" ).html( "+" );
		});
	
/*-------------------------------------------------------------------
	DONATIONS
--------------------------------------------------------------------*/

		$( ".donate-now-btn" ).click(function() {
		  $( "#donate_now_form" ).submit();
		});

/*-------------------------------------------------------------------
	MEMBERSHIP
--------------------------------------------------------------------*/

		$( "#membership-accordion #form_renewal_option_lifetime" ).click( function() {
			$( "#membership-accordion #annual-membership-options" ).hide();
			$( "#membership-accordion #lifetime-membership-options" ).show();
			$( "#membership-accordion #subscription" ).hide();
			$( "#membership-accordion #payment_method_subscription" ).prop( "checked", false );
			$( "#membership-accordion #payment_method_paypal" ).prop( "checked", true );
			$( "#membership-accordion #payment_method_pledge" ).prop( "checked", false );
			$( "#membership-accordion #subscription-text" ).hide();
			$( "#membership-accordion #paypal-text" ).show();
			$( "#membership-accordion #pledge-text" ).hide();
		});
		$( "#membership-accordion #form_renewal_option_annual" ).click( function() {

			var annopt = $( "input[name='form_annual_option']:checked" ).val();

			$( "#membership-accordion #lifetime-membership-options" ).hide();
			$( "#membership-accordion #annual-membership-options" ).show();

			if( annopt == "Student" ) {
				$( "#membership-accordion #subscription" ).hide();
				$( "#membership-accordion #payment_method_subscription" ).prop( "checked", false );
				$( "#membership-accordion #subscription-text" ).hide();
				$( "#membership-accordion #payment_method_paypal" ).prop( "checked", true );
				$( "#membership-accordion #payment_method_pledge" ).prop( "checked", false );
				$( "#membership-accordion #paypal-text" ).show();
				$( "#membership-accordion #pledge-text" ).hide();
			} else {
				$( "#membership-accordion #subscription" ).show();
				$( "#membership-accordion #payment_method_subscription" ).prop( "checked", true );
				$( "#membership-accordion #subscription-text" ).show();
				$( "#membership-accordion #payment_method_paypal" ).prop( "checked", false );
				$( "#membership-accordion #payment_method_pledge" ).prop( "checked", false );
				$( "#membership-accordion #paypal-text" ).hide();
				$( "#membership-accordion #pledge-text" ).hide();
			}

		});
		$( "#membership-accordion .student" ).click( function() {
			$( "#membership-accordion #spouse_name" ).hide();
			$( "#membership-accordion #individual_title" ).hide();
			$( "#membership-accordion #family_title" ).hide();
			$( "#membership-accordion #student_title" ).show();
			$( "#membership-accordion #lifetime-membership-options" ).hide();
			$( "#membership-accordion #annual-membership-options" ).show();
			$( "#membership-accordion #subscription" ).hide();
			$( "#membership-accordion #payment_method_subscription" ).prop( "checked", false );
			$( "#membership-accordion #subscription-text" ).hide();
			$( "#membership-accordion #payment_method_paypal" ).prop( "checked", true );
			$( "#membership-accordion #payment_method_pledge" ).prop( "checked", false );
			$( "#membership-accordion #paypal-text" ).show();
			$( "#membership-accordion #pledge-text" ).hide();
		});
		$( "#membership-accordion .family" ).click( function() {
			$( "#membership-accordion #spouse_name" ).show();
			$( "#membership-accordion #individual_title" ).hide();
			$( "#membership-accordion #student_title" ).hide();
			$( "#membership-accordion #family_title" ).show();
			$( "#membership-accordion #subscription" ).show();
			$( "#membership-accordion #payment_method_subscription" ).prop( "checked", true );
			$( "#membership-accordion #subscription-text" ).show();
			$( "#membership-accordion #payment_method_paypal" ).prop( "checked", false );
			$( "#membership-accordion #payment_method_pledge" ).prop( "checked", false );
			$( "#membership-accordion #paypal-text" ).hide();
			$( "#membership-accordion #pledge-text" ).hide();
		});
		$( "#membership-accordion .individual" ).click( function() {
			$( "#membership-accordion #spouse_name" ).hide();
			$( "#membership-accordion #student_title" ).hide();
			$( "#membership-accordion #family_title" ).hide();
			$( "#membership-accordion #individual_title" ).show();
			$( "#membership-accordion #subscription" ).show();
			$( "#membership-accordion #payment_method_subscription" ).prop( "checked", true );
			$( "#membership-accordion #subscription-text" ).show();
			$( "#membership-accordion #payment_method_paypal" ).prop( "checked", false );
			$( "#membership-accordion #payment_method_pledge" ).prop( "checked", false );
			$( "#membership-accordion #paypal-text" ).hide();
			$( "#membership-accordion #pledge-text" ).hide();
		});
		$( "#membership-accordion #payment_method_subscription" ).click( function() {
			$( "#membership-accordion #paypal-text" ).hide();
			$( "#membership-accordion #pledge-text" ).hide();
			$( "#membership-accordion #subscription-text" ).show();
		});
		$( "#membership-accordion #payment_method_paypal" ).click( function() {
			$( "#membership-accordion #subscription-text" ).hide();
			$( "#membership-accordion #pledge-text" ).hide();
			$( "#membership-accordion #paypal-text" ).show();
		});
		$( "#membership-accordion #payment_method_pledge" ).click( function() {
			$( "#membership-accordion #subscription-text" ).hide();
			$( "#membership-accordion #paypal-text" ).hide();
			$( "#membership-accordion #pledge-text" ).show();
		});
		$( "#membership-accordion #form_member_phone" ).change( function() {
			var formattedPhoneNbr = formatPhone( $( "#membership-accordion #form_member_phone" ).val() );
			$( "#membership-accordion #form_member_phone" ).val( formattedPhoneNbr );
		});
		$( "#membership-accordion #form_member_alt_phone" ).change( function() {
			var formattedPhoneNbr = formatPhone( $( "#membership-accordion #form_member_alt_phone" ).val() );
			$( "#membership-accordion #form_member_alt_phone" ).val( formattedPhoneNbr );
		});
		$( "#membership-accordion #form_member_bday_mm" ).change( function() {
			var bdayMM = $( "#form_member_bday_mm" ).val();

			if( bdayMM == "01"
			||  bdayMM == "03"
			||  bdayMM == "05"
			||  bdayMM == "07"
			||  bdayMM == "08"
			||  bdayMM == "10"
			||  bdayMM == "12" ) {
				$( "#membership-accordion #dd30" ).show();
				$( "#membership-accordion #dd31" ).show();
			}

			if( bdayMM == "04"
			||  bdayMM == "06"
			||  bdayMM == "09"
			||  bdayMM == "11" ) {
				$( "#membership-accordion #dd30" ).show();
				$( "#membership-accordion #dd31" ).hide();
			}

			if( bdayMM == "02" ) {
				$( "#membership-accordion #dd30" ).hide();
				$( "#membership-accordion #dd31" ).hide();
			}
		});

/*-------------------------------------------------------------------
	FLICKR
--------------------------------------------------------------------*/

		$( "#flickr-btn-large" ).mouseover( function() {
			$( "#flickr-btn-large" ).prop( 'src','./img/flickr-btn-large-hover.png' );
		});
		$( "#flickr-btn-large" ).mouseout( function() {
			$( "#flickr-btn-large" ).prop( 'src','./img/flickr-btn-large.png' );
		});

/*-------------------------------------------------------------------
	SUBSCRIBE
--------------------------------------------------------------------*/

		$( "#cc-subscribe-btn" ).mouseover( function() {
			$( "#cc-subscribe-btn" ).prop( 'src','./img/cc-subscribe-btn-hover.png' );
		});
		$( "#cc-subscribe-btn" ).mouseout( function() {
			$( "#cc-subscribe-btn" ).prop( 'src','./img/cc-subscribe-btn.png' );
		});

/*-------------------------------------------------------------------
	FUNCTIONS
--------------------------------------------------------------------*/

		function formatPhone( v ) {

			if( v.length > 7 ) {
				v = v.replace(/\s/g,"");
				v = v.replace(/\(/g,"");
				v = v.replace(/\)/g,"");
				v = v.replace(/\-/g,"");
				v = v.replace(/\./g,"");
			}

			tel = v;

			if( v.length == 10 ) {
				tel  = "(";
				tel += v.substr(0,3);
				tel += ") ";
				tel += v.substr(3,3);
				tel += "-";
				tel += v.substr(6,4);
			}

			if( v.length == 7 ) {
				tel  = v.substr(0,3);
				tel += "-";
				tel += v.substr(3,4);
			}
					
			return tel;
		}

/*-------------------------------------------------------------------
	END
--------------------------------------------------------------------*/

	} )

} )( jQuery );