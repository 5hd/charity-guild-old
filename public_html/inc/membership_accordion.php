				<form name="membership-form" id="membership-form" method="post" action="#membership">
				<ul id="membership-accordion">
					<li id="membership_application">

						<h3><span>+</span>Membership Application&nbsp;/ Renewal</h3>
						<div id="membershipAppContent" class="content <?php echo $membership_accordion_display_class; ?>">

						<fieldset>
							<h4 id="membership_options">Membership Options</h4>
							<div class="radio input-section" id="new-renew-options">
								<input type="radio" name="form_new_renew_option" id="form_new_renew_option_new"   value="New Membership"<?php if( $form_new_renew_option != "Membership Renewal" ) : ?> checked="checked"<?php endif; ?>><label for="form_new_renew_option_new">New Membership</label><br />
								<input type="radio" name="form_new_renew_option" id="form_new_renew_option_renew" value="Membership Renewal"<?php if( $form_new_renew_option == "Membership Renewal" ) : ?> checked="checked"<?php endif; ?>><label for="form_new_renew_option_renew">Membership Renewal</label>
							</div>
							<hr />
							<div class="radio input-section" id="renewal-options">
								<input type="radio" name="form_renewal_option" id="form_renewal_option_annual" value="Annual"<?php if( $form_renewal_option != "Lifetime" ) : ?> checked="checked"<?php endif; ?>><label for="form_renewal_option_annual">Annual</label><br />
								<input type="radio" name="form_renewal_option" id="form_renewal_option_lifetime" value="Lifetime"<?php if( $form_renewal_option == "Lifetime" ) : ?> checked="checked"<?php endif; ?>><label for="form_renewal_option_lifetime">Lifetime</label>
							</div>
							<hr />
							<div class="input-section<?php echo $annual_membership_display_class; ?>" id="annual-membership-options">
								<div class="radio">
									<input type="radio" name="form_annual_option" id="form_annual_option_student" class="student" value="Student"<?php if( $form_annual_option == "Student" ) : ?> checked="checked"<?php endif; ?>><label for="form_annual_option_student">Student:&nbsp;&nbsp; $5</label><br />
									<input type="radio" name="form_annual_option" id="form_annual_option_individual" class="individual" value="Individual"<?php if( $form_annual_option != "Family" ) : ?> checked="checked"<?php endif; ?>><label for="form_annual_option_individual">Individual:&nbsp;&nbsp; $25</label><br />
									<input type="radio" name="form_annual_option" id="form_annual_option_family" class="family" value="Family"<?php if( $form_annual_option == "Family" ) : ?> checked="checked"<?php endif; ?>><label for="form_annual_option_family">Family:&nbsp;&nbsp; $40</label>
								</div>
							</div>
							<div class="input-section<?php echo $lifetime_membership_display_class; ?>" id="lifetime-membership-options">
								<div class="radio">
									<input type="radio" name="form_lifetime_option" id="form_lifetime_option_individual" class="individual" value="Individual"<?php if( $form_lifetime_option != "Family" ) : ?> checked="checked"<?php endif; ?>><label for="form_lifetime_option_individual">Individual:&nbsp;&nbsp; $225</label><br />
									<input type="radio" name="form_lifetime_option" id="form_lifetime_option_family" class="family" value="Family"<?php if( $form_lifetime_option == "Family" ) : ?> checked="checked"<?php endif; ?>><label for="form_lifetime_option_family">Family:&nbsp;&nbsp; $400</label>
								</div>
							</div>
							<br />
						</fieldset>

						<fieldset id="individual_name">
							<h4 id="individual_title">Individual Membership</h4>
							<h4 id="family_title" class="invisible">Family Membership</h4>
							<h4 id="student_title" class="invisible">Student Membership</h4>
							<label for="form_member_title"<?php if($merror['form_member_title']) : ?> class="error"<?php endif; ?>>Title:</label><br />
							<input type="text" name="form_member_title" id="form_member_title" value="<?php echo $form_member_title; ?>" style="max-width: 100px;" /><br /><br />
							<label for="form_member_firstname" class="required<?php if($merror['form_member_firstname']) : ?>  error"<?php endif; ?>">First Name:</label><br />
							<input type="text" name="form_member_firstname" id="form_member_firstname" value="<?php echo $form_member_firstname; ?>" /><br /><br />
							<label for="form_member_lastname" class="required<?php if($merror['form_member_lastname']) : ?> error<?php endif; ?>">Last Name:</label><br />
							<input type="text" name="form_member_lastname" id="form_member_lastname" value="<?php echo $form_member_lastname; ?>" /><br /><br />
							<label for="form_member_birthday"<?php if($merror['form_member_birthday']) : ?>  class="error"<?php endif; ?>>Birthday:</label><br />
							<select name="form_member_bday_mm" id="form_member_bday_mm" class="dropdown" style="font-family: courier, fixed; font-size: 1em;">
								<option value="00" disabled="disabled"<?php if( $form_member_bday_mm == "00" ) : ?> selected="selected"<?php endif; ?>>MM</option>
								<option value="01" id="mm01"<?php if( $form_member_bday_mm == "01" ) : ?> selected="selected"<?php endif; ?>>01</option>
								<option value="02" id="mm02"<?php if( $form_member_bday_mm == "02" ) : ?> selected="selected"<?php endif; ?>>02</option>
								<option value="03" id="mm03"<?php if( $form_member_bday_mm == "03" ) : ?> selected="selected"<?php endif; ?>>03</option>
								<option value="04" id="mm04"<?php if( $form_member_bday_mm == "04" ) : ?> selected="selected"<?php endif; ?>>04</option>
								<option value="05" id="mm05"<?php if( $form_member_bday_mm == "05" ) : ?> selected="selected"<?php endif; ?>>05</option>
								<option value="06" id="mm06"<?php if( $form_member_bday_mm == "06" ) : ?> selected="selected"<?php endif; ?>>06</option>
								<option value="07" id="mm07"<?php if( $form_member_bday_mm == "07" ) : ?> selected="selected"<?php endif; ?>>07</option>
								<option value="08" id="mm08"<?php if( $form_member_bday_mm == "08" ) : ?> selected="selected"<?php endif; ?>>08</option>
								<option value="09" id="mm09"<?php if( $form_member_bday_mm == "09" ) : ?> selected="selected"<?php endif; ?>>09</option>
								<option value="10" id="mm10"<?php if( $form_member_bday_mm == "10" ) : ?> selected="selected"<?php endif; ?>>10</option>
								<option value="11" id="mm11"<?php if( $form_member_bday_mm == "11" ) : ?> selected="selected"<?php endif; ?>>11</option>
								<option value="12" id="mm12"<?php if( $form_member_bday_mm == "12" ) : ?> selected="selected"<?php endif; ?>>12</option>
							</select>
							&nbsp;/&nbsp;
							<select name="form_member_bday_dd" id="form_member_bday_dd" class="dropdown" style="font-family: courier, fixed; font-size: 1em;">
								<option value="00" disabled="disabled"<?php if( $form_member_bday_dd == "00" ) : ?> selected="selected"<?php endif; ?>>DD</option>
								<option value="01" id="dd01"<?php if( $form_member_bday_dd == "01" ) : ?> selected="selected"<?php endif; ?>>01</option>
								<option value="02" id="dd02"<?php if( $form_member_bday_dd == "02" ) : ?> selected="selected"<?php endif; ?>>02</option>
								<option value="03" id="dd03"<?php if( $form_member_bday_dd == "03" ) : ?> selected="selected"<?php endif; ?>>03</option>
								<option value="04" id="dd04"<?php if( $form_member_bday_dd == "04" ) : ?> selected="selected"<?php endif; ?>>04</option>
								<option value="05" id="dd05"<?php if( $form_member_bday_dd == "05" ) : ?> selected="selected"<?php endif; ?>>05</option>
								<option value="06" id="dd06"<?php if( $form_member_bday_dd == "06" ) : ?> selected="selected"<?php endif; ?>>06</option>
								<option value="07" id="dd07"<?php if( $form_member_bday_dd == "07" ) : ?> selected="selected"<?php endif; ?>>07</option>
								<option value="08" id="dd08"<?php if( $form_member_bday_dd == "08" ) : ?> selected="selected"<?php endif; ?>>08</option>
								<option value="09" id="dd09"<?php if( $form_member_bday_dd == "09" ) : ?> selected="selected"<?php endif; ?>>09</option>
								<option value="10" id="dd10"<?php if( $form_member_bday_dd == "10" ) : ?> selected="selected"<?php endif; ?>>10</option>
								<option value="11" id="dd11"<?php if( $form_member_bday_dd == "11" ) : ?> selected="selected"<?php endif; ?>>11</option>
								<option value="12" id="dd12"<?php if( $form_member_bday_dd == "12" ) : ?> selected="selected"<?php endif; ?>>12</option>
								<option value="13" id="dd13"<?php if( $form_member_bday_dd == "13" ) : ?> selected="selected"<?php endif; ?>>13</option>
								<option value="14" id="dd14"<?php if( $form_member_bday_dd == "14" ) : ?> selected="selected"<?php endif; ?>>14</option>
								<option value="15" id="dd15"<?php if( $form_member_bday_dd == "15" ) : ?> selected="selected"<?php endif; ?>>15</option>
								<option value="16" id="dd16"<?php if( $form_member_bday_dd == "16" ) : ?> selected="selected"<?php endif; ?>>16</option>
								<option value="17" id="dd17"<?php if( $form_member_bday_dd == "17" ) : ?> selected="selected"<?php endif; ?>>17</option>
								<option value="18" id="dd18"<?php if( $form_member_bday_dd == "18" ) : ?> selected="selected"<?php endif; ?>>18</option>
								<option value="19" id="dd19"<?php if( $form_member_bday_dd == "19" ) : ?> selected="selected"<?php endif; ?>>19</option>
								<option value="20" id="dd20"<?php if( $form_member_bday_dd == "20" ) : ?> selected="selected"<?php endif; ?>>20</option>
								<option value="21" id="dd21"<?php if( $form_member_bday_dd == "21" ) : ?> selected="selected"<?php endif; ?>>21</option>
								<option value="22" id="dd22"<?php if( $form_member_bday_dd == "22" ) : ?> selected="selected"<?php endif; ?>>22</option>
								<option value="23" id="dd23"<?php if( $form_member_bday_dd == "23" ) : ?> selected="selected"<?php endif; ?>>23</option>
								<option value="24" id="dd24"<?php if( $form_member_bday_dd == "24" ) : ?> selected="selected"<?php endif; ?>>24</option>
								<option value="25" id="dd25"<?php if( $form_member_bday_dd == "25" ) : ?> selected="selected"<?php endif; ?>>25</option>
								<option value="26" id="dd26"<?php if( $form_member_bday_dd == "26" ) : ?> selected="selected"<?php endif; ?>>26</option>
								<option value="27" id="dd27"<?php if( $form_member_bday_dd == "27" ) : ?> selected="selected"<?php endif; ?>>27</option>
								<option value="28" id="dd28"<?php if( $form_member_bday_dd == "28" ) : ?> selected="selected"<?php endif; ?>>28</option>
								<option value="29" id="dd29"<?php if( $form_member_bday_dd == "29" ) : ?> selected="selected"<?php endif; ?>>29</option>
								<option value="30" id="dd30"<?php if( $form_member_bday_dd == "30" ) : ?> selected="selected"<?php endif; ?>>30</option>
								<option value="31" id="dd31"<?php if( $form_member_bday_dd == "31" ) : ?> selected="selected"<?php endif; ?>>31</option>
							</select>
							<br /><br />
						</fieldset>

						<fieldset id="spouse_name"<?php echo $spouse_name_display_class; ?>>
							<h4>Spouse</h4>
							<label for="form_spouse_title"<?php if($merror['form_spouse_title']) : ?> class="error"<?php endif; ?>>Title:</label><br />
							<input type="text" name="form_spouse_title" id="form_spouse_title" value="<?php echo $form_spouse_title; ?>" style="max-width: 100px;" /><br /><br />
							<label for="form_spouse_firstname" class="required<?php if($merror['form_spouse_firstname']) : ?> error<?php endif; ?>">Spouse First Name</label><br />
							<input type="text" name="form_spouse_firstname" id="form_spouse_firstname" value="<?php echo $form_spouse_firstname; ?>" /><br /><br />
							<label for="form_spouse_lastname" class="required<?php if($merror['form_spouse_lastname']) : ?> error<?php endif; ?>">Spouse Last Name:</label><br />
							<input type="text" name="form_spouse_lastname" id="form_spouse_lastname" value="<?php echo $form_spouse_lastname; ?>" />
							<br /><br />
						</fieldset>

						<fieldset id="business_name"<?php echo $business_name_display_class; ?>>
							<br />
							<label for="form_business_name" class="required<?php if($merror['form_business_name']) : ?> error<?php endif; ?>">Business Name:</label><br />
							<input type="text" name="form_business_name" id="form_business_name" value="<?php echo $form_business_name; ?>" />
							<br /><br />
						</fieldset>

						<fieldset>
							<h4>Address</h4>

							<label for="form_member_address1" class="required<?php if($merror['form_member_address1']) : ?> error<?php endif; ?>">Address 1:</label><br />
							<input type="text" name="form_member_address1" id="form_member_address1" value="<?php echo $form_member_address1; ?>" /><br /><br />

							<label for="form_member_address2"<?php if($merror['form_member_address2']) : ?> class="error"<?php endif; ?>>Address 2:</label><br />
							<input type="text" name="form_member_address2" id="form_member_address2" value="<?php echo $form_member_address2; ?>" /><br /><br />

							<label for="form_member_city" class="required<?php if($merror['form_member_city']) : ?> error<?php endif; ?>">City:</label><br />
							<input type="text" name="form_member_city" id="form_member_city" value="<?php echo $form_member_city; ?>" /><br /><br />

							<label for="form_member_state" class="required<?php if($merror['form_member_state']) : ?> error<?php endif; ?>">State:</label><br />
							<select name="form_member_state" id="form_member_state" class="dropdown" style="font-family: courier, fixed; font-size: 1em;">
								<option value="AL"<?php if( $form_member_state == "AL" ) : ?> selected="selected"<?php endif; ?>>AL &ndash; Alabama</option>
								<option value="AK"<?php if( $form_member_state == "AK" ) : ?> selected="selected"<?php endif; ?>>AK &ndash; Alaska</option>
								<option value="AZ"<?php if( $form_member_state == "AZ" ) : ?> selected="selected"<?php endif; ?>>AZ &ndash; Arizona</option>
								<option value="AR"<?php if( $form_member_state == "AR" ) : ?> selected="selected"<?php endif; ?>>AR &ndash; Arkansas</option>
								<option value="CA"<?php if( $form_member_state == "CA" ) : ?> selected="selected"<?php endif; ?>>CA &ndash; California</option>
								<option value="CO"<?php if( $form_member_state == "CO" ) : ?> selected="selected"<?php endif; ?>>CO &ndash; Colorado</option>
								<option value="CT"<?php if( $form_member_state == "CT" ) : ?> selected="selected"<?php endif; ?>>CT &ndash; Connecticut</option>
								<option value="DE"<?php if( $form_member_state == "DE" ) : ?> selected="selected"<?php endif; ?>>DE &ndash; Delaware</option>
								<option value="FL"<?php if( $form_member_state == "FL" ) : ?> selected="selected"<?php endif; ?>>FL &ndash; Florida</option>
								<option value="GA"<?php if( $form_member_state == "GA" ) : ?> selected="selected"<?php endif; ?>>GA &ndash; Georgia</option>
								<option value="HI"<?php if( $form_member_state == "HI" ) : ?> selected="selected"<?php endif; ?>>HI &ndash; Hawaii</option>
								<option value="ID"<?php if( $form_member_state == "ID" ) : ?> selected="selected"<?php endif; ?>>ID &ndash; Idaho</option>
								<option value="IL"<?php if( $form_member_state == "IL" ) : ?> selected="selected"<?php endif; ?>>IL &ndash; Illinois</option>
								<option value="IN"<?php if( $form_member_state == "IN" ) : ?> selected="selected"<?php endif; ?>>IN &ndash; Indiana</option>
								<option value="IA"<?php if( $form_member_state == "IA" ) : ?> selected="selected"<?php endif; ?>>IA &ndash; Iowa</option>
								<option value="KS"<?php if( $form_member_state == "KS" ) : ?> selected="selected"<?php endif; ?>>KS &ndash; Kansas</option>
								<option value="KY"<?php if( $form_member_state == "KY" ) : ?> selected="selected"<?php endif; ?>>KY &ndash; Kentucky</option>
								<option value="LA"<?php if( $form_member_state == "LA" ) : ?> selected="selected"<?php endif; ?>>LA &ndash; Louisiana</option>
								<option value="ME"<?php if( $form_member_state == "ME" ) : ?> selected="selected"<?php endif; ?>>ME &ndash; Maine</option>
								<option value="MD"<?php if( $form_member_state == "MD" ) : ?> selected="selected"<?php endif; ?>>MD &ndash; Maryland</option>
								<option value="MA"<?php if( $form_member_state == "MA" || empty( $form_member_state ) ) : ?> selected="selected"<?php endif; ?>>MA &ndash; Massachusetts</option>
								<option value="MI"<?php if( $form_member_state == "MI" ) : ?> selected="selected"<?php endif; ?>>MI &ndash; Michigan</option>
								<option value="MN"<?php if( $form_member_state == "MN" ) : ?> selected="selected"<?php endif; ?>>MN &ndash; Minnesota</option>
								<option value="MS"<?php if( $form_member_state == "MS" ) : ?> selected="selected"<?php endif; ?>>MS &ndash; Mississippi</option>
								<option value="MO"<?php if( $form_member_state == "MO" ) : ?> selected="selected"<?php endif; ?>>MO &ndash; Missouri</option>
								<option value="MT"<?php if( $form_member_state == "MT" ) : ?> selected="selected"<?php endif; ?>>MT &ndash; Montana</option>
								<option value="NE"<?php if( $form_member_state == "NE" ) : ?> selected="selected"<?php endif; ?>>NE &ndash; Nebraska</option>
								<option value="NV"<?php if( $form_member_state == "NV" ) : ?> selected="selected"<?php endif; ?>>NV &ndash; Nevada</option>
								<option value="NH"<?php if( $form_member_state == "NH" ) : ?> selected="selected"<?php endif; ?>>NH &ndash; New Hampshire</option>
								<option value="NJ"<?php if( $form_member_state == "NJ" ) : ?> selected="selected"<?php endif; ?>>NJ &ndash; New Jersey</option>
								<option value="NM"<?php if( $form_member_state == "NM" ) : ?> selected="selected"<?php endif; ?>>NM &ndash; New Mexico</option>
								<option value="NY"<?php if( $form_member_state == "NY" ) : ?> selected="selected"<?php endif; ?>>NY &ndash; New York</option>
								<option value="NC"<?php if( $form_member_state == "NC" ) : ?> selected="selected"<?php endif; ?>>NC &ndash; North Carolina</option>
								<option value="ND"<?php if( $form_member_state == "ND" ) : ?> selected="selected"<?php endif; ?>>ND &ndash; North Dakota</option>
								<option value="OH"<?php if( $form_member_state == "OH" ) : ?> selected="selected"<?php endif; ?>>OH &ndash; Ohio</option>
								<option value="OK"<?php if( $form_member_state == "OK" ) : ?> selected="selected"<?php endif; ?>>OK &ndash; Oklahoma</option>
								<option value="OR"<?php if( $form_member_state == "OR" ) : ?> selected="selected"<?php endif; ?>>OR &ndash; Oregon</option>
								<option value="PA"<?php if( $form_member_state == "PA" ) : ?> selected="selected"<?php endif; ?>>PA &ndash; Pennsylvania</option>
								<option value="RI"<?php if( $form_member_state == "RI" ) : ?> selected="selected"<?php endif; ?>>RI &ndash; Rhode Island</option>
								<option value="SC"<?php if( $form_member_state == "SC" ) : ?> selected="selected"<?php endif; ?>>SC &ndash; South Carolina</option>
								<option value="SD"<?php if( $form_member_state == "SD" ) : ?> selected="selected"<?php endif; ?>>SD &ndash; South Dakota</option>
								<option value="TN"<?php if( $form_member_state == "TN" ) : ?> selected="selected"<?php endif; ?>>TN &ndash; Tennessee</option>
								<option value="TX"<?php if( $form_member_state == "TX" ) : ?> selected="selected"<?php endif; ?>>TX &ndash; Texas</option>
								<option value="UT"<?php if( $form_member_state == "UT" ) : ?> selected="selected"<?php endif; ?>>UT &ndash; Utah</option>
								<option value="VT"<?php if( $form_member_state == "VT" ) : ?> selected="selected"<?php endif; ?>>VT &ndash; Vermont</option>
								<option value="VA"<?php if( $form_member_state == "VA" ) : ?> selected="selected"<?php endif; ?>>VA &ndash; Virginia</option>
								<option value="WA"<?php if( $form_member_state == "WA" ) : ?> selected="selected"<?php endif; ?>>WA &ndash; Washington</option>
								<option value="WV"<?php if( $form_member_state == "WV" ) : ?> selected="selected"<?php endif; ?>>WV &ndash; West Virginia</option>
								<option value="WI"<?php if( $form_member_state == "WI" ) : ?> selected="selected"<?php endif; ?>>WI &ndash; Wisconsin</option>
								<option value="WY"<?php if( $form_member_state == "WY" ) : ?> selected="selected"<?php endif; ?>>WY &ndash; Wyoming</option>

								<option value="00" disabled="disabled">Commonwealth/Territory:</option>
								<option value="AS"<?php if( $form_member_state == "AS" ) : ?> selected="selected"<?php endif; ?>>AS &ndash; American Samoa</option>
								<option value="DC"<?php if( $form_member_state == "DC" ) : ?> selected="selected"<?php endif; ?>>DC &ndash; District of Columbia</option>
								<option value="FM"<?php if( $form_member_state == "FM" ) : ?> selected="selected"<?php endif; ?>>FM &ndash; Fed. ST Micronesia</option>
								<option value="GU"<?php if( $form_member_state == "GU" ) : ?> selected="selected"<?php endif; ?>>GU &ndash; Guam</option>
								<option value="MH"<?php if( $form_member_state == "MH" ) : ?> selected="selected"<?php endif; ?>>MH &ndash; Marshall Islands</option>
								<option value="MP"<?php if( $form_member_state == "MP" ) : ?> selected="selected"<?php endif; ?>>MP &ndash; No. Mariana Islands</option>
								<option value="PW"<?php if( $form_member_state == "PW" ) : ?> selected="selected"<?php endif; ?>>PW &ndash; Palau</option>
								<option value="PR"<?php if( $form_member_state == "PR" ) : ?> selected="selected"<?php endif; ?>>PR &ndash; Puerto Rico</option>
								<option value="VI"<?php if( $form_member_state == "VI" ) : ?> selected="selected"<?php endif; ?>>VI &ndash; Virgin Islands</option>

								<option value="00" disabled="disabled">Military:</option>
								<option value="AE"<?php if( $form_member_state == "AE" ) : ?> selected="selected"<?php endif; ?>>AE &ndash; AF Africa</option>
								<option value="AA"<?php if( $form_member_state == "AA" ) : ?> selected="selected"<?php endif; ?>>AA &ndash; AF Americas</option>
								<option value="AE"<?php if( $form_member_state == "AE" ) : ?> selected="selected"<?php endif; ?>>AE &ndash; AF Canada</option>
								<option value="AE"<?php if( $form_member_state == "AE" ) : ?> selected="selected"<?php endif; ?>>AE &ndash; AF Europe</option>
								<option value="AE"<?php if( $form_member_state == "AE" ) : ?> selected="selected"<?php endif; ?>>AE &ndash; AF Middle East</option>
								<option value="AP"<?php if( $form_member_state == "AP" ) : ?> selected="selected"<?php endif; ?>>AP &ndash; AF Pacific</option>
							</select><br /><br />

							<label for="form_member_zip" class="required<?php if($merror['form_member_zip']) : ?> error<?php endif; ?>">Zip Code:</label><br />
							<input type="text" maxlength="10" name="form_member_zip" id="form_member_zip" value="<?php echo $form_member_zip; ?>" style="max-width: 100px;" /><br />

							<br /><br />
						</fieldset>

						<fieldset>
							<h4>Phone and Email</h4>

							<label for="form_member_phone" class="required<?php if($merror['form_member_phone']) : ?> error<?php endif; ?>">Phone Number:</label><br />
							<input class="phone-number-mask" type="tel" name="form_member_phone" id="form_member_phone" value="<?php echo $form_member_phone; ?>" onblur="this.value=formatPhone(this.value);" /><br /><br />

							<label for="form_member_alt_phone"<?php if($merror['form_member_alt_phone']) : ?> class="error"<?php endif; ?>>Alt Phone Number:</label><br />
							<input class="phone-number-mask" type="tel" name="form_member_alt_phone" id="form_member_alt_phone" value="<?php echo $form_member_alt_phone; ?>" onblur="this.value=formatPhone(this.value);" /><br /><br />

							<label for="form_member_emailaddr" class="required<?php if($merror['form_member_emailaddr']) : ?> error<?php endif; ?>">Email Address:</label><br />
							<input type="email" name="form_member_emailaddr" id="form_member_emailaddr" value="<?php echo $form_member_emailaddr; ?>" />

							<br /><br />
						</fieldset>

						<fieldset>

							<div class="radio input-section">
								<h4>Payment Method</h4>
								<div id="subscription">
									<input type="radio" name="form_payment_method" id="payment_method_subscription" value="Subscription"<?php if( $form_payment_method != "PayPal" && $form_payment_method != "Pledge" ) : ?> checked="checked"<?php endif; ?>><label for="payment_method_subscription"><b>PayPal</b><span class="smaller"> - Automatic Annual Renewal</span></label><br /><br />
								</div>
								<input type="radio" name="form_payment_method" id="payment_method_paypal" value="PayPal"<?php if( $form_payment_method == "PayPal" ) : ?> checked="checked"<?php endif; ?>><label for="payment_method_paypal"><b>PayPal</b><span class="smaller"> - One Time Payment</span></label><br /><br />
								<input type="radio" name="form_payment_method" id="payment_method_pledge" value="Pledge"<?php if( $form_payment_method == "Pledge" ) : ?> checked="checked"<?php endif; ?>><label for="payment_method_pledge"><b>Pledge</b><span class="smaller"> - Mail in a check</span></label><br />
							</div>

							<div class="input-section">
								<p id="subscription-text"<?php echo $subscription_text_display_class; ?> style="margin: 0 auto; padding: 5px; max-width: 240px; text-align: center; border: 1px solid #e0e0e0;">You will be transferred to PayPal<br />to complete your<br /><span class='highlight'>Automatic Annual Renewal</span>.</p>
								<p id="paypal-text"<?php echo $paypal_text_display_class; ?> style="margin: 0 auto; padding: 5px; max-width: 240px; text-align: center; border: 1px solid #e0e0e0;">You will be transferred to PayPal to complete your one-time transaction.</p>
								<p id="pledge-text"<?php echo $pledge_text_display_class; ?> style="margin: 0 auto; padding: 5px; max-width: 240px; text-align: center; border: 1px solid #e0e0e0;">A membership application will be displayed that you can print and send in with your membership fee.</p>
							</div>

						</fieldset>

						<fieldset>
							<div style="margin: 0 auto; text-align: center; max-width: 250px;">
								<div id="mrecaptcha" class="g-recaptcha" style="<?php if($error['m-recaptcha']) : ?>border: 3px solid red; <?php endif; ?>transform:scale(0.85);-webkit-transform:scale(0.85);transform-origin:0 0;-webkit-transform-origin:0 0;"></div>
							</div>
						</fieldset>


						<fieldset id="membership_submit">
							<p class="center"><input type="submit" name="msubmit" value="Submit" /></p>
							<p class="center"><a class="close" href="">Close</a></p>
						</fieldset>

						</div>
					</li>
				</ul>
				</form>
