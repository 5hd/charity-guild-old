<?php

/*-----------------------------------------------
	DEFAULTS
-----------------------------------------------*/

$home_url = "https://thecharityguild.org";

/*-----------------------------------------------
	RECAPTCHA
-----------------------------------------------*/

require_once( "./inc/recaptchalib.php" );

$secret = "6LcM0DMUAAAAAAzP6rDYYujvyNyMnStJMlXw-ltQ";
$response = null;
$reCaptcha = new ReCaptcha($secret);

/*-----------------------------------------------
	SET UP PHP MAILER
-----------------------------------------------*/

require_once ( 'PHPMailerAutoload.php' );


/*-----------------------------------------------
	THE MESSAGE
-----------------------------------------------*/

$msg = true;

$themsg = ""
	.	"<p class='ttl'>COVID-19 UPDATE: as of Oct 2020</p>" . "\r"
	.	"<p class='bold center' style='margin: 0 auto; padding: 7px; border: 1px solid black; border-radius: 5px; max-width: 200px;'>The Food Pantry is Open:<br /><br />TUESDAY &nbsp;&nbsp;10AM - 12PM<br />WEDNESDAY &nbsp;&nbsp;10AM - 12PM<br />THURSDAY &nbsp;&nbsp;10AM - 12PM</p>" . "\r"
	.	"<p><strong>Our Thrift Shop is now OPEN</strong> on Monday, Friday & Saturday from 10am-3pm. To schedule a <strong>donation drop off day</strong> please email us: <a href='mailto:donate.2tcg@gmail.com'>donate2tcg@gmail.com</a></p>" . "\r"
	.	"<p>As COVID-19 updates are presented daily, our food pantry team is working diligently to stay informed, compliant, safe and operational.</p>" . "\r"
	.	"<p>In an effort to comply with social distancing guidelines, we have modified our client intake process.</p>" . "\r"
	.	"<p class=bold center'>During this time, clients are not entering our food pantry.</p>" . "\r"
	.	"<p>We ask clients to arrive early and please expect longer wait times. We appreciate everyone's cooperation and patience at this challenging time.</p>" . "\r"
	.	"<p class='bold green'>Please note that this information is subject to change daily.</span></p>" . "\r"
	.	"<p class='bold'>For current information please visit our <a href='https://facebook.com/thecharityguild' target='_blank'>facebook</a> & <a href='https://instagram.com/thecharityguild' target='_blank'>instagram</a> pages.</p>" . "\r";

/*-----------------------------------------------
	DONATIONS
-----------------------------------------------*/

if( isset( $_GET['dp'] ) ) {

	$pp_stat = $_GET['dp'];

	if( $pp_stat == "s" ) {
		$dresult_msg = "<p class=\"success\">Thank you for your donation.<br />Check your email for a confirmation from PayPal.</p>";
	} else {
		$dresult_msg = "<p class=\"error\">Your PayPal transaction has been cancelled.</p>";
	}

	$donation_symbol = "-";
	$donation_display_class = "block";
} else {
	$donation_symbol = "+";
	$donation_display_class = "hidden";
}

/*-----------------------------------------------
	MEMBERSHIP
-----------------------------------------------*/

include( "./inc/membership_process.php" );

$membership_symbol = "+";
$membership_display_class = "hidden";

$membership_accordion_symbol = "+";
$membership_accordion_display_class = "hidden";

if( isset( $_GET['mp'] )
|| 	isset( $_GET['mpledge'] )
|| 	isset( $_POST['msubmit'] ) ) {
	$membership_symbol = "-";
	$membership_display_class = "block";
}

if( isset( $_POST['msubmit'] ) && $errors_exist ) {
	$membership_accordion_symbol = "-";
	$membership_accordion_display_class = "block";
}

/*-----------------------------------------------
	SEND AN EMAIL
-----------------------------------------------*/

if( isset( $_POST['esubmit'] ) ) {

include( "./inc/sendemail_process.php" );

	$sendemail_symbol = "-";
	$sendemail_display_class = "block";
} else {
	$sendemail_symbol = "+";
	$sendemail_display_class = "hidden";
}

/*-----------------------------------------------
	R E C E I P T
-----------------------------------------------*/
?>
<?php if( $receipt ) : ?>
<html>
<head>
<title>The Charity Guild, Inc.</title>
<link rel="stylesheet" href="styles.css" type="text/css" media="all" />
</head>
<body class="receipt">
<div id="receipt-content">

<?php 	if( strtolower( $form_payment_method ) == "subscription" ) : ?>
			<h2 class="success" style="margin: 25px auto; text-align: center; font-family: arial, sans-serif; font-weight: normal;">Transferring to PayPal ...</h2>
<?php 		if ( isset( $_POST['msubmit'] ) ) : ?>
<?php 			include( "./inc/membership_paypal.php" ); ?>
<?php 		endif; ?>
<?php 	endif; ?>

<?php 	if( strtolower( $form_payment_method ) == "paypal" ) : ?>
			<h2 class="success" style="margin: 25px auto; text-align: center; font-family: arial, sans-serif; font-weight: normal;">Transferring to PayPal ...</h2>
<?php 		if ( isset( $_POST['msubmit'] ) ) : ?>
<?php 			include( "./inc/membership_paypal.php" ); ?>
<?php 		endif; ?>
<?php 	endif; ?>

<?php 	if( strtolower( $form_payment_method ) == "pledge" ) : ?>
<?php 		if ( isset( $_POST['msubmit'] ) ) : ?>
<?php 			include( "./inc/membership_receipt.php" ); ?>
<?php 		endif; ?>
<?php 	endif; ?>

</div><!-- END #receipt-content -->
<?php else : ?>
<?php
/*-----------------------------------------------
	B E G I N   P A G E
-----------------------------------------------*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>The Charity Guild | Food Pantry 02301, Food Pantry in Brockton MA, Free Food in Brockton MA, Thrift Shop in Brockton MA, Thrift Store in Brockton MA, Volunteer in Brockton MA, Donate to The Charity Guild in Brockton MA, Volunteer Opportunity in Brockton MA, Hunger Agency in Brockton MA, Social Services in Brockton MA, Hunger Services in Brockton MA, Brockton Human Services</title>
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="robots" content="index, follow" />
<meta name="author" content="Brite Ventures" />
<meta name="contact" content="info@thecharityguild.org" />
<meta name="copyright" content="Copyright <?php echo date('Y') ?> The Charity Guild, Inc." />
<meta name="keywords" content="Food Pantry 02301, Food Pantry in Brockton MA, Free Food in Brockton MA, Thrift Shop in Brockton MA, Thrift Store in Brockton MA, Volunteer in Brockton MA, Donate to The Charity Guild in Brockton MA, Volunteer Opportunity in Brockton MA, Hunger Agency in Brockton MA, Social Services in Brockton MA, Hunger Services in Brockton MA, Brockton Human Services" />
<meta name="description" content="The Charity Guild is a food pantry and thrift shop located at 501 Main Street in Brockton MA, serving the greater Brockton area.  Free Food is provided to our clients on Tuesday, Wednesday, and Thursday at 10:00 a.m.  The thrift shop carries new and gently used designer and name brand clothing, accessories, giftware, and household goods at bargain prices.  The thrift shop is open Monday through Saturday from 10:00 a.m. to 3:00 p.m." />
<meta http-equiv="X-UA-Compatible" content="IE=9" />
<link rel="shortcut icon" href="./img/favicon.ico" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type='text/javascript' src="./inc/functions.js"></script>
<link rel='stylesheet' type='text/css' href='styles.css' media='all' />
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
</head>

<body>

<div id="header">
	<div id="masthead">
		<img id="logomural" src="./img/cg-circle-mural.jpg" alt="" />
	</div>
</div>

<div id="gbfbtop">
	<a href="http://gbfb.org" title="GBFB.org" target="_blank"><img src="./img/GBFB_ProudMemberAgency.gif" alt="Greater Boston Food Bank Proud Member Agency" /></a>
</div>

<div id="wrapper">

<?php if( $msg ) : ?>
	<div class="themsg">
		<?php echo $themsg; ?>
	</div>

<?php endif; ?>
	<div class="donatenow">
		<p class="center"><a class="donate-now-btn purple-button" href="javascript:void();">Click here to DONATE NOW!</a></p>
	</div>

	<div id="facebook">
		<a href="http://facebook.com/thecharityguild" target="_blank"><img src="./img/joinusonfb-btn.png" onmouseover="this.src='./img/joinusonfb-hover-btn.png'" onmouseout="this.src='./img/joinusonfb-btn.png'" /></a>
	</div>

	<div id="gbfbwrap">
		<a href="http://gbfb.org" title="GBFB.org" target="_blank"><img src="./img/GBFB_ProudMemberAgency.gif" alt="Greater Boston Food Bank Proud Member Agency" /></a>
	</div>

	<ul id="accordion">

<!-- ABOUT -->

		<a name="about"></a>
		<li id="about">
			<h2 id="ttlAbout"><span>+</span>About The Charity Guild</h2>
			<div id="aboutContent" class="content hidden">
				<h3>About The&nbsp;Charity&nbsp;Guild</h3>
				<p>The&nbsp;Charity&nbsp;Guild was formed in 1971 by a group of concerned individuals whose goal was simple &ndash; to provide clothing and food for those in need. Four decades later, our mission remains the same:</p>
				<p class="mission"><b>"</b>&nbsp;To provide basic and necessary services to those unable to meet their primary needs of food, clothing and household goods.<b>"</b></p>
				<p>Our Food Pantry and Thrift Shop located at 501 Main Street in Brockton, MA, serve the greater Brockton area.</p>
				<p>Our Thrift Shop offers quality new and gently used designer and name brand clothing at reasonable prices.</p>
				<p>We have coats, clothing and shoes for men, women and children, as well as a wide variety of gift ware, housewares, home d&eacute;cor, small appliances, books and jewelry.</p>
				<p>The Thrift Shop is open Monday through Saturday, from 10:00 a.m. to 3:00 p.m.</p>
				<p>The funds raised from the sale of goods, together with donations, grants and other fundraising, support the large emergency food pantry we operate.</p>
				<p>More than 14,000 people receive food from our pantry each year. Some 33% are children and 19% are elderly.</p>
				<p>Individuals and families are eligible to receive perishable items such as meat, eggs, bread, fresh fruit and vegetables, and non-perishable items such as canned goods, cereal and pasta.</p>
				<p>In addition to the basic foods, we often have &quot;extras&quot; such as pastries, crackers, raisins and trail mix which we offer to clients.</p>
				<p class="center">The&nbsp;Charity&nbsp;Guild,&nbsp;Inc. is a<br />non-profit 501 (c) (3) organization.<br />Donations are tax-deductible.<br />Receipts are given on request.</p>
				<p class="center"><a class="close" href="">Close</a></p>
			</div>
		</li>

<!-- DONATIONS -->

		<a name="donations"></a>
		<li id="donations">
			<h2 id="ttlDonations"><span><?php echo $donation_symbol; ?></span>Donations</h2>
			<div id="donationsContent" class="content <?php echo $donation_display_class; ?>">
				<br />
				<h3 class="center">Donations to The&nbsp;Charity&nbsp;Guild are always welcome.</h3>
				<p class="center">The&nbsp;Charity&nbsp;Guild, Inc. is a<br />non-profit 501 (c) (3) organization.<br />Donations are tax-deductible.<br />Receipts are given on request.</p>
				<hr />

<!-- 	Monetary Donations -->

<?php if( $dresult_msg ) : ?><?php echo $dresult_msg . "\n"; ?><?php endif; ?>
				<h3>Monetary Donations</h3>
				<p>The&nbsp;Charity&nbsp;Guild accepts monetary donations of any size.</p>
				<p class="bold">Please mail donations and inquiries to:</p>
				<p class="indent">The Charity Guild, Inc.<br />P.O. Box 4856<br />Brockton, MA 02303-4856</p>
				<p>You can also use a credit card or your PayPal account to make donations online.</p>
				<p class="center"><img id="paypal-logo-cards" style="width: 200px; height: 73px;" src="./img/paypal-logo-cards.png" /></p>
				<p class="bold ppblue">One-Time PayPal Donations:</p>
				<ul class="disc">
				<li><p>Is this donation for a specific <span class="bold">Event</span> or purpose?</p></li>
				<li><p>Is this donation <span class="bold">In Honor Of</span> or <span class="bold">In Memory Of</span> someone?</p></li>
				<li><p>Would you like an <span class="bold">Acknowledgement</span> of this donation to be sent to someone?</p></li>
				<li class="edit">Just click on the icon to provide any specifics in the space provided at PayPal.</li>
				</ul>
				<p class="bold ppblue">Monthly PayPal Donations:</p>
				<ul class="disc">
				<li><p>Would you like to participate in our <span class="bold green">Monthly Donor Program</span>?</p>
					<div class="serif italic small">
					<p>In addition to our mortgage, utilities, insurance and other business-related expenses, we have significant food costs.</p>
					<p>Though we are able to acquire many items very reasonably from The Greater Boston Food Bank, we supplement their supplies and other donations with items we purchase from local wholesalers.</p>
					<p>In order to provide healthy bags of food to our clients, we buy meat, eggs, margarine, cheese, and other perishable goods to supplement the pasta, rice, cereal, peanut butter, powdered milk, and canned goods we place in the bags.</p>
					<p>Your monthly gift to The Charity Guild becomes part of the three day food supply we give our clients.</p>
					<p>Thank you.</p>
					</div>
				</li>
				<li class="check">Simply check <span class="bold">"Make this a monthly donation"</span> at PayPal.</li>
				</ul>
				<p class="bold ppblue">Just one more thing:</p>
				<ul class="check">
				<li><p>Please check <span class="bold">&quot;Share my mailing address with The&nbsp;Charity&nbsp;Guild.&quot;</span></p></li>
				</ul>
				<p class="thankyou">Thank you!</p>
				<p class="center"><a class="donate-now-btn purple-button" href="javascript:void();">PayPal Donate Now!</a></p>
				<br /><hr />

<!-- 	Food Pantry Donations -->

				<h3>Food Pantry Donations</h3>
				<p>Non-perishable food donations are accepted Monday through Friday 10:00 a.m. to 3:00 p.m.</p>
				<p>For large donations, please contact us to arrange a time.</p>
				<p class="thankyou">Thank you!</p>
				<br /><hr />

<!-- 	Thrift Shop Donations -->

				<h3>Thrift Shop Donations</h3>
				<p>We accept:</p>
				<ul class="disc">
				<li><p>Clean, usable clothing in excellent condition only! (No tears, holes, stains or missing buttons.) We believe the dignity of our customers is important. We will not sell anything that our volunteers would not wear. We also accept shoes, handbags and suitcases.</p></li>
				<li><p>Small household goods and appliances in good condition (toaster ovens, coffee makers, lamps, etc.). We also accept dishes, cooking utensils, glassware, and other kitchen items.</p></li>
				<li><p>Clean, usable gift ware in good condition.</p></li>
				<li><p>Clean sheets, blankets, and pillowcases in good condition.</p></li>
				<li><p>Project Undercover: New, unopened packages of socks & underwear for men, women and children.</p></li>
				</ul>
				<p>Thrift Shop donations are accepted <span class="bold">Mondays, Fridays, and Saturdays</span>, during regular business hours.</p>
				<p class="thankyou">Thank you!</p>
				<br />
				<p class="center"><a class="close" href="">Close</a></p>
			</div>
		</li>

<!-- FOOD PANTRY -->

		<a name="foodpantry"></a>
		<li id="foodpantry">
			<h2 id="ttlFoodPantry"><span>+</span>The Food Pantry</h2>
			<div id="foodpantryContent" class="content hidden">
				<div class="floatbox"><p class="title">The Food Pantry is Open</p><p>Tuesday, Wednesday & Thursday<br />10:00 a.m. &ndash; 12:00 noon<br /><span class="red">ARRIVE EARLY!</span></p></div>
				<h3>About the Food Pantry</h3>
				<p>The Food Pantry distributes both perishable and non-perishable foods to individuals and families requesting assistance.</p>
				<p>Individuals and families are eligible to receive perishable items such as meat, eggs, bread, fresh fruit and vegetables, and non-perishable items such as canned goods, cereal and pasta.</p>
				<p>In addition to the basic foods, we often have &quot;extras&quot; such as pastries, crackers, raisins and trail mix which we offer to clients.</p>
				<p>There are many different activities required to make the Food Pantry run smoothly.</p>
				<p>Volunteers help with client intake, filling bags, redistributing bulk foods, stocking shelves, sorting donations or assisting with record keeping.</p>
				<a id="gbfbfp" href="http://gbfb.org" target="_blank" title="GBFB.org"><img src="./img/GBFB_ProudMemberAgency.gif" alt="Greater Boston Food Bank Proud Member Agency" /></a>
				<p>We purchase food from a local wholesaler, obtain items from The Greater Boston Food Bank, and accept donations from individuals and companies.</p>
				<p>Similar to a regular retail grocer, these items must be stocked and rotated to assure that our clients receive the freshest food we have available.</p>
				<h4>Donations:</h4>
				<p>Non-perishable food donations are accepted Monday through Friday 10:00 a.m. to 3:00 p.m.</p>
				<p>For large donations, contact us to arrange a time.</p>
				<p class="thankyou">Thank you!</p>
				<p class="center"><a class="close" href="">Close</a></p>
			</div>
		</li>

<!-- THRIFT SHOP -->

		<a name="thriftshop"></a>
		<li id="thriftshop">
			<h2 id="ttlThriftShop"><span>+</span>The Thrift Shop</h2>
			<div id="thriftshopContent" class="content hidden">
				<div class="floatbox"><p class="title">The Thrift Shop is Open</p><p>Monday, Friday & Saturday<br />10:00 a.m. &ndash; 3:00 p.m.</p></div>
				<h3>About the Thrift Shop</h3>
				<p>The&nbsp;Charity&nbsp;Guild Thrift Shop offers high quality, new and gently used designer and brand name clothing for men, women and children at reasonable prices.</p>
				<p>We carry clothing by many of the top designers such as <span class="bold darkgreen smallcaps">Liz Claiborne, Talbot&apos;s, Harv&eacute; Benard, Jones New York</span> and <span class="bold darkgreen smallcaps">DKNY</span>.  We also have shoes and handbags from <span class="bold darkgreen smallcaps">Dooney and Bourke, Etienne Aigner</span> and <span class="bold darkgreen smallcaps">Prada</span>.</p>
				<div class="greenbox floatleft">Our Thrift Shop is<br />&quot;the best-kept Secret<br />in Brockton&quot;!</div>
				<p>We carry a wide variety of gift ware, household goods, small appliances, books, and jewelry. There is something for everyone and inventory is always changing.</p>
				<p>Our store stock changes each day as new donations are put out for display by our volunteers.</p>
				<p><span class="bold darkgreen smallcaps">Villeroy &amp; Boch, Lenox,</span> and <span class="bold darkgreen smallcaps">Pfaltzgraff</span> are found along with everyday, useful items.</p>
				<p>Quality is excellent and pricing is very affordable.</p>
				<h3><a title="Favorite us on eBay!" href="https://www.charity.ebay.com/charity/The-Charity-Guild/193554" target="_blank">Our eBay Listings</a></h3>
				<p>The&nbsp;Charity&nbsp;Guild uses eBay&apos;s Giving Works program to occasionally list Thrift Shop items for sale &ndash; so you can conveniently shop online!</p>
				<p class="center"><a target="_blank" href="https://www.charity.ebay.com/charity/The-Charity-Guild/193554"><img class="ebayfavbtn" src="./img/btn-ebay-fav-us.png" alt="Favorite us on eBay"></a></p>
				<div style="margin: 0 auto; text-align: center;"><iframe style="margin: 0 auto; border: none;" src="//www.auctionnudge.com/ad_build/iframe/SellerID/thecharityguild/siteid/0/format/250x250/theme/green/MaxEntries/20/carousel_auto/5/blank_noitems/0/hide_username/0/sortOrder/BestMatch" width="250" height="250" frameborder="0"></iframe></div>
				<h3>How You Can Help</h3>
				<p>The funds raised from sales in the Thrift Shop, together with donations, grants and other fundraising, support our large emergency Food Pantry.</p>
				<p>You can support our Thrift Shop by shopping there often, and by donating gently used clothing and housewares in good condition.</p>
				<p>Thrift Shop donations are accepted <span class="bold highlight">Mondays, Fridays, and Saturdays,</span> during regular business hours.</p>
				<p>Project Undercover: Donate new, unopened packages of socks and underwear for men, women and children.</p>
				<p>We also have volunteer opportunities in the Thrift Shop, as well as in the Food Pantry.</p>
				<p class="thankyou">Thank You!</p>
				<p class="center"><a class="close" href="">Close</a></p>
			</div>
		</li>

<!-- VOLUNTEER -->

		<a name="volunteer"></a>
		<li id="volunteer">
			<h2 id="ttlVolunteer"><span>+</span>Volunteer</h2>
			<div id="volunteerContent" class="content hidden">
				<img id="blue-bee-feathered" class="floatright" style="width: 250px;" src="./img/blue-bee-feathered.png" alt="" />
				<h3>The Missing Ingredient is YOU!</h3>
				<p>The&nbsp;Charity&nbsp;Guild needs volunteers <span class="highlight">18 and older</span> to work in our Food Pantry and Thrift Shop.</p>
				<h4>Volunteer in the Food Pantry</h4>
				<p>The Food Pantry hours are 10:00 a.m. to 12:00 Noon on Tuesdays, Wednesdays and Thursdays.  Help stock shelves, refrigerators and freezers.  Assemble custom grocery bags and distribute them to individuals and families.  Work one day, two days or all three days to provide food to the needy in the Brockton area.</p>
				<h4>Volunteer in the Thrift Shop</h4>
				<p>Our Thrift Shop needs volunteers to help sort through donated clothing and other items  Steam, shelve and hang clothing, freshen and shelve household items, and help with pricing too!  Open Monday through Saturday 10:00 a.m. to 3:00 p.m., we need volunteers at least 3 hours per day, one day per week.</p>
				<h4>Students</h4>
				<p>Students <i>may</i> be able to complete their required Community Service hours in the Food Pantry or Thrift Shop.</p>
				<p class="serif italic">Any student under the age of 18 must be accompanied by a parent, teacher or mentor while they are completing their service in the Food Pantry or Thrift Shop. Please call us to discuss your service and arrange your hours at least two weeks before your anticipated start date.</p>
				<h4>No Experience Necessary!</h4>
				<p>By enlisting the aid of many volunteers, we are able to keep expenses low and concentrate funds on providing food to families and individuals in the greater Brockton area.</p>
				<div class="docbox floatleft"><a href="./doc/Volunteer-Application.pdf" target="_blank"><img style="margin: 0 auto; max-height: 100px; border: 1px solid black;" src="./img/Volunteer-Application-pdf-image.jpg" alt="Volunteer Application" /><br />Volunteer<br />Application</a></div>
				<p>The benefits of helping in the shop or pantry are significant. It is a very hands-on experience and volunteers actually see and talk to the people they are helping. Knowing that you are touching the lives of so many in such a positive way is a truly rewarding experience (and we have a lot of fun).</p>
				<p><span class="highlight">Join Our Relaxed, Fun Environment</span> for just one day, then decide how often you&apos;d like to take part in this fulfilling work.</p>
				<p><span class="highlight">Hours are Flexible</span> and you have the opportunity to help both the Charity Guild and our clients.</p>
				<p>Contact us for more information.</p>
				<h4 class="center">Stop in and visit us!<br />See what we do and why we are<br />one of the largest food pantries<br />on the South Shore.</h4>
				<p class="center"><a class="close" href="">Close</a></p>
			</div>
		</li>

<!-- MEMBERSHIP -->

		<a name="membership"></a>
		<li id="membership">
			<h2 id="ttlMembership"><span><?php echo $membership_symbol; ?></span>Membership</h2>
			<div id="membershipContent" class="content <?php echo $membership_display_class; ?>">
<?php if( $mresult_msg ) : ?><br /><?php echo "\t\t\t\t" . $mresult_msg . "\n"; ?><?php endif; ?>
				<img class="floatright" src="./img/group-of-women-1.png" style="max-width: 280px;" />
				<h3>Benefits of Membership</h3>
				<p>Being a Member of The Charity Guild makes you an important part of the work we do in the community.</p>
				<ul class="disc">
				<li><p><b>Earn</b>: A 10% discount on all items in our Thrift Shop.</p></li>
				<li><p><b>Receive</b>:&nbsp; Newsletter updates about our Food Pantry and Thrift Shop.</p></li>
				<li><p><b>Attend</b>:&nbsp; Special events like our Gala and Annual Meeting.</p></li>
				<li><p><b>Volunteer</b>:&nbsp; At events or in our Food Pantry and/or Thrift Shop.</p></li>
				<li><p><b>Connect</b>:&nbsp; With interesting, like-minded people.</p></li>
				<li><p><b>Improve</b>:&nbsp; The Brockton Community!</p></li>
				</ul>
				<p>Members can take comfort in knowing that they are helping us to achieve our mission.</p>
				<br />
				<div style="margin: 0 auto; text-align: center;">
					<div class="display-inline" style="margin: 0 20px;">
						<table class="member-options-table">
						<tr><th colspan="2">Annual Membership</th></tr>
						<tr><td>Student:</td><td class="right">$5</td></tr>
						<tr><td>Individual:</td><td class="right">$25</td></tr>
						<tr><td>Family:</td><td class="right">$40</td></tr>
						</table>
					</div>
					<div class="display-inline" style="margin: 0 20px;">
						<table class="member-options-table">
						<tr><th colspan="2">Life Membership</th></tr>
						<tr><td>Individual:</td><td class="right">$225</td></tr>
						<tr><td>Family:</td><td class="right">$400</td></tr>
						</table>
					</div>
				</div>

<!-- 	Membership Application / Renewal -->

<?php include( "./inc/membership_accordion.php" ); ?>
				<p class="center"><a class="close" href="">Close</a></p>
			</div>
		</li>

<!-- NEWSLETTERS -->

		<a name="newsletters"></a>
		<li id="newsletters">
			<h2 id="ttlNewsletters"><span>+</span>Newsletters</h2>
			<div id="newslettersContent" class="content hidden">
				<h3 class="whatshap">What's Happening At The&nbsp;Charity&nbsp;Guild</h3>
				<h4 class="center">Newsletter Archive</h4>
				<div style="margin: 0 auto; padding: 0; max-width: 650px; text-align: center;">
<?php
					$dir = "./newsletters/";
					$newsletters = array_diff( scandir( $dir, 1 ), array( '..', '.', 'index.php', 'error_log' ) );
					foreach( $newsletters as $newsletter ) {
						$v = strpos( $newsletter, 'v' ) + 1;
						$n = strpos( $newsletter, 'n', 1 ) + 1;
						$vol = (int)substr( $newsletter, $v, 2 );
						$no = (int)substr( $newsletter, $n, 2 );
						$title = "Vol. " . $vol . " No. " . $no;
						echo '<span class="newsletter"><a href="' . $dir . $newsletter . '" target="_blank">' . $title . '</a></span>';
					}
?>
					<p style="clear: both;">&nbsp;</p>
				</div>
				<p class="center"><a class="close" href="">Close</a></p>
			</div>
		</li>

<!-- PHOTOS -->

		<a name="photos"></a>
		<li id="photos">
			<h2 id="ttlPhotoAlbums"><span>+</span>Photo Albums</h2>
			<div id="photosContent" class="content hidden">
				<h3>Our Photo Albums</h3>
				<p class="center"><a href="https://www.flickr.com/photos/thecharityguild/sets/" target="_blank" /><img id="flickr-btn-large" src="./img/flickr-btn-large.png" style="width: 205px; height: 97px;" alt="Access our Flickr Photo Albums" /></a></p>
				<p class="center"><a href="https://www.instagram.com/thecharityguild/" target="_blank" /><img id="instagram-btn-large" src="./img/instagramr-btn-large.png" style="width: 205px; height: 97px;" alt="Follow us on Instagram!" /></a></p>
				<p class="center"><a class="close" href="">Close</a></p>
			</div>
		</li>

<!-- EBAY -->

		<a name="ebay"></a>
		<li id="ebay">
			<h2 id="ttlEbay"><span>+</span>Ebay</h2>
			<div id="ebayContent" class="content hidden">
				<h3>Our Ebay Listings</h3>
				<p>The&nbsp;Charity&nbsp;Guild uses eBay&apos;s Giving Works program to occasionally list Thrift Shop items for sale &ndash; so you can conveniently shop online!</p>
				<p>When we have items listed on eBay, a sampling will be shown below.</p>
				<br />
				<p class="center bold green">Don&apos;t forget to Favorite us!</p>
				<p class="center"><a target="_blank" href="https://www.charity.ebay.com/charity/The-Charity-Guild/193554"><img class="ebayfavbtn" src="./img/btn-ebay-fav-us.png" alt="Favorite us on eBay"></a></p>
				<br />
				<hr />
				<h4><a href="https://www.charity.ebay.com/charity/The-Charity-Guild/193554" target="_blank">Visit us on eBay</a> for a complete listing.</h4>
				<script type="text/javascript" src="//www.auctionnudge.com/item_build/js/SellerID/thecharityguild/siteid/0/theme/details/MaxEntries/20/show_logo/1/sortOrder/BestMatch"></script>
				<div id="auction-nudge-items" class="auction-nudge"><a href="http://www.auctionnudge.com/your-ebay-items">Active eBay Listings From Auction Nudge</a></div>
				<p><br /></p>
				<p class="center"><a class="close" href="">Close</a></p>
			</div>
		</li>

<!-- SUBSCRIBE -->

		<a name="subscribe"></a>
		<li id="subscribe">
			<h2 id="ttlSubscribe"><span>+</span>Subscribe</h2>
			<div id="subscribeContent" class="content hidden">
				<h3 class="center">Let's Keep in Touch</h3>
				<p class="center"><img id="keyboard-touch" style="max-width: 100%;" src="./img/keyboard-touch.png" alt="" /></p>
				<p>Subscribe to our email list to receive notifications of events, newsletters, photo&nbsp;galleries and more!</p>
				<br />
				<p class="center"><a href="https://visitor.r20.constantcontact.com/d.jsp?llr=l5osit8ab&p=oi&m=1129149184971&sit=6ou7it7lb&f=8ba4bc7f-65eb-492d-8027-c53104de0b87" target="_blank"><img id="cc-subscribe-btn" style="width: 180px; height: 50px;" src="./img/cc-subscribe-btn.png" alt="Subscribe to our email list" /></a></p>
				<br />
				<p class="center"><a class="close" href="">Close</a></p>
			</div>
		</li>

<!-- FOLLOW US -->

		<a name="follow"></a>
		<li id="follow">
			<h2 id="ttlFollow"><span>+</span>Follow Us</h2>
			<div id="followContent" class="content hidden">
				<h3 class="center">Follow Us!</h3>
				<table id="followtable">
				<tr>
					<td class="icon"><a href="http://facebook.com/thecharityguild" target="_blank"><img src="./img/facebook-icon.png" alt="" /></a></td>
					<td class="furl"><a href="http://facebook.com/thecharityguild" target="_blank">facebook.com/thecharityguild</a></td>
				</tr>
				<tr>
					<td class="icon"><a href="http://instagram.com/thecharityguild" target="_blank"><img src="./img/instagram-icon.png" alt="" /></a></td>
					<td class="furl"><a href="http://instagram.com/thecharityguild" target="_blank">instagram.com/thecharityguild</a></td>
				</tr>
				<tr>
					<td class="icon"><a href="https://www.charity.ebay.com/charity/The-Charity-Guild/193554" target="_blank"><img src="./img/ebay-icon.png" alt="" /></a></td>
					<td class="furl"><a href="https://www.charity.ebay.com/charity/The-Charity-Guild/193554" target="_blank">bit.ly/cgebay</a></td>
				</tr>
				<tr>
					<td class="icon"><a href="https://www.flickr.com/photos/thecharityguild/albums" target="_blank"><img src="./img/flickr-icon.png" alt="" /></a></td>
					<td class="furl"><a href="https://www.flickr.com/photos/thecharityguild/albums" target="_blank">flickr.com/thecharityguild</a></td>
				</tr>
				</table>
				<br />
				<p class="center"><a class="close" href="">Close</a></p>
			</div>
		</li>

<!-- SENDEMAIL -->

		<a name="sendemail"></a>
		<li id="sendemail">
			<h2 id="ttlSendEmail"><span><?php echo $sendemail_symbol; ?></span>Send An Email</h2>
			<div id="sendanemailContent" class="content <?php echo $sendemail_display_class; ?>">
				<p><br /></p>
<?php if( $eresult_msg ) : ?><?php echo $eresult_msg . "\n"; ?><br /><?php endif; ?>
				<div id="sendemail_container">
					<form id="sendemail_form" name="sendemail_form" method="post" action="#sendemail" />
					<label for="first_name"<?php if($error['first_name']) : ?> class="error"<?php endif; ?>>First&nbsp;Name:</label><br />
					<input type="text" name="first_name" id="first_name" value="<?php echo $first_name; ?>" /><br /><br />
					<label for="last_name"<?php if($error['last_name']) : ?> class="error"<?php endif; ?>>Last&nbsp;Name:</label><br />
					<input type="text" name="last_name" id="last_name" value="<?php echo $last_name; ?>" /><br /><br />
					<label for="email_address"<?php if($error['email_address']) : ?> class="error"<?php endif; ?>>Email&nbsp;Address:</label><br />
					<input type="email" name="email_address" id="email_address" value="<?php echo $email_address; ?>" /><br /><br />
					<label for="subject"<?php if($error['subject']) : ?> class="error"<?php endif; ?>>Subject:</label><br />
					<input type="text" name="subject" id="subject" value="<?php echo $subject; ?>" maxlength="50" /><br /><br />
					<label for="message"<?php if($error['message']) : ?> class="error"<?php endif; ?>>Message:</label><br />
					<textarea name="message" id="message" rows="10" style="width: 100%;" scroll="auto"><?php echo $message; ?></textarea><br /><br />
					<div style="margin: 0 auto; text-align: center; max-width: 250px;">
						<div id="erecaptcha" class="g-recaptcha" style="<?php if($error['e-recaptcha']) : ?>border: 3px solid red; <?php endif; ?>transform:scale(0.85);-webkit-transform:scale(0.85);transform-origin:0 0;-webkit-transform-origin:0 0;"></div>
					</div>
					<p class="center"><input type="submit" name="esubmit" value="Send" /></p>
					</form>
				</div>
				<p><br /></p>
				<p class="center"><a class="close" href="">Close</a></p>
				<br />
			</div>
		</li>
	</ul><!-- END #accordion -->

<!-- CONTENT FOOTER -->

	<div id="contact">
		<p><a href="https://goo.gl/maps/EUvNJ" target="_blank"><img id="gicon" src="./img/google-maps-icon.png" alt="" /></a></p>
		<p>The Charity Guild, Inc.<br />501 Main Street<br />P.O. Box 4856<br />Brockton, MA 02303-4856</p>
		<p>P:&nbsp; <a href="tel:5085835280">(508) 583-5280</a><br />F:&nbsp; (508) 583-6680</p>
		<p><a href=""><img src="./img/cg-circle.png" style="max-width: 50px; border: none; text-decoration: none;" /></a></p>
	</div>

</div><!-- END #wrapper -->

<!-- FOOTER -->

<div id="footer">
	&nbsp;
</div>

<!-- PAYPAL FORM -->

<div class="invisible">
	<form id="donate_now_form" name="donate_now_form" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
	<input type="hidden" name="cmd" value="_s-xclick">
	<input type="hidden" name="hosted_button_id" value="DLTAFFHPFAZFU">
	<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
	<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
	</form>
</div>

<!-- STATCOUNTER -->

<script type="text/javascript">
var sc_project=5641948;
var sc_invisible=1;
var sc_security="a7f990a3";
</script>
<script type="text/javascript" src="https://www.statcounter.com/counter/counter.js" async></script>
<noscript><div class="statcounter"><a title="Web Analytics" href="http://statcounter.com/" target="_blank"><img class="statcounter" src="//c.statcounter.com/5641948/0/a7f990a3/1/" alt="Web Analytics"></a></div></noscript>

<!-- USE 2 RECAPTCHAS ON ONE PAGE -->

<script type="text/javascript" charset="utf-8">

var onloadCallback = function() {

	var recaptchas = document.querySelectorAll('div[class=g-recaptcha]');

	for( i = 0; i < recaptchas.length; i++) {
		grecaptcha.render( recaptchas[i].id, {
			'sitekey' : '6LcM0DMUAAAAABslOkfxSTFNm8wo1D5XNXu_6v94',
		});
	}
}

</script>

<?php endif; ?>
</body>
</html>

