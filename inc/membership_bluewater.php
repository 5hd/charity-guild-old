<?php

//--------------------------------------------------------------
// 	Process Bluewater MX Merchant
// 	URLs created at MX Merchant on 9/13/2014
//--------------------------------------------------------------

$mx_url = array(
	"annual individual"   => "https://mxcustomer.com/d/3e8611e7-71c4-44a9-a964-490627b2e7d1",
	"annual family"       => "https://mxcustomer.com/d/fe96e0d1-228c-4fa6-a503-29106182f6c6",
	"annual corporate"    => "https://mxcustomer.com/d/ad0e3b50-5d1c-4881-b73f-19355c284421",
	"lifetime individual" => "https://mxcustomer.com/d/c5233083-b45f-4f9a-8deb-1fe90a42215e",
	"lifetime family"     => "https://mxcustomer.com/d/7b65d87b-20f8-4afe-b4d3-38c542c4acf7",
	"lifetime corporate"  => "https://mxcustomer.com/d/52cf1287-87a6-4901-9fbf-61f33ffd22fb",
);

$fee_amt = array(
	"annual individual"   => "20.00",
	"annual family"       => "35.00",
	"annual corporate"    => "200.00",
	"lifetime individual" => "200.00",
	"lifetime family"     => "350.00",
	"lifetime corporate"  => "2,000.00",
);

$form_member_address = empty( $form_member_address2 ) ? $form_member_address1 : $form_member_address1 . ", " . $form_member_address2;

$actionkey = strtolower( $form_renewal_option ) . " " . strtolower( $form_member_option );

$actionurl = $mx_url[$actionkey];

$membership_fee = $fee_amt[$actionkey];

?>
<form name='mxform' id='mxform' method="get" action="<?php echo $actionurl; ?>">
<input type="hidden" name="SHOWHEADER" value="1" />
<input type="hidden" name="CUSTOMERNAME" value="<?php echo $member_name; ?>" />
<input type="hidden" name="ADDRESS" value="<?php echo $form_member_address; ?>" />
<input type="hidden" name="CITY" value="<?php echo $form_member_city; ?>" />
<input type="hidden" name="STATE" value="<?php echo $form_member_state; ?>" />
<input type="hidden" name="ZIP" value="<?php echo $form_member_zip; ?>" />
<input type="hidden" name="EMAIL" value="<?php echo $form_member_emailaddr; ?>" />
<input type="hidden" name="PHONE" value="<?php echo $form_member_phone; ?>" />
<input type="hidden" name="AMT" value="<?php echo $membership_fee; ?>" />
<input type="hidden" name="ALLOWPARTIAL" value="0" />
</form>

<script>document.getElementById('mxform').submit()</script>
