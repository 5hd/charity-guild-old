<?php

$fee_amt = array(
	"annual student"      => "$5",
	"annual individual"   => "$25",
	"annual family"       => "$40",
	"lifetime individual" => "$225",
	"lifetime family"     => "$400",
);

$fee_key = strtolower( $form_renewal_option ) . " " . strtolower( $form_member_option );

$membership_fee = $fee_amt[$fee_key];

$receipt = 0;

?>
			<div class="dock">

				<p class="center noprint"><a href="https://thecharityguild.org?mpledge=s#membership">&laquo; Back to The Charity Guild Website</a></p>

				<h2 class="center">Your Membership <?php echo $appnoun; ?></h2>
				<p class="center noprint">Please <a href="javascript:print();">print this pledge</a><br />and mail it with your membership fee.</p>

				<h3>Membership Option:</h3>
				<p><?php echo $form_new_renew_option; ?></p>
				<p><?php echo $form_renewal_option; ?> <?php echo $form_member_option; ?> Membership:  <?php echo $membership_fee; ?></p>

				<h3>Contact Info:</h3>
				<p><?php echo $member_name; ?><br />
<?php if( $form_member_option == "Family" ) : ?>
				<?php echo $spouse_name; ?><br />
<?php endif; ?>
<?php if( $form_member_option == "Student" ) : ?>
				<b><?php echo $form_member_name; ?></b><br />
<?php endif; ?>
				<?php echo $form_member_address1; ?><br />
<?php if( !empty( $form_member_address2 ) ) : ?>
				<?php echo $form_member_address2; ?><br />
<?php endif; ?>
				<?php echo $form_member_city; ?>, <?php echo $form_member_state; ?> <?php echo $form_member_zip; ?></p>

				<p><?php echo $form_member_phone; ?><br />
<?php if( !empty( $form_member_alt_phone ) ) : ?>
				<?php echo $form_member_alt_phone; ?><br />
<?php endif; ?>
				<?php echo $form_member_emailaddr; ?></p>

<?php if( $the_bday ) : ?>
				<h3>Birthday:</h3>
<?php 	if( $form_member_option == "Family" ) : ?>
				<p><?php echo stripslashes( $member_name ); ?>'s birthday is <?php echo $the_bday; ?>.</p>
<?php 	else : ?>
				<p><?php echo $the_bday; ?></p>
<?php 	endif; ?>
<?php endif; ?>

				<div class="cgaddy">
					<p class="center">Please mail your<br /><?php echo $membership_fee; ?> Membership Fee to:</p>
					<p class="center">The Charity Guild, Inc.<br />P.O. Box 4856<br />Brockton, MA 02303-4856</p>
				</div>

				<h2 class="center thankyou">Thank You!</h2>

				<p><br /></p>

			</div>
